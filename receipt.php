<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    
<?php
$order_id = $_GET['order_id'];
include_once('includes/condb.php');
$sql = "SELECT * FROM tbl_order WHERE order_id = '$order_id'";
$result = mysqli_query($conn, $sql);
$data = mysqli_fetch_array($result);
$allsum = 0;
?>

<table width="100%" border="0">
  <tr>
    <td colspan="6" align="center"><h3>ใบสั่งซื้อเลขที่ <?=$order_id?></h3><hr>
</td>
  </tr>
  <tr>
    <td colspan="6" align="right"><a onClick="window.print() ;" href="#">พิมพ์ใบสั่งซื้อ</a></td>
  </tr>
  <tr>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
  </tr>
  <tr>
    <td width="5%" align="center">#</td>
    <td width="15%" align="center">รูป</td>
    <td width="10%" align="center">ชื่อ</td>
    <td width="8%" align="center">ราคา</td>
    <td width="8%" align="center">จำนวน</td>
    <td width="8%" align="center">ราคารวม</td>
  </tr>
<?php
$no = 1;
$sql_re = "SELECT * FROM tbl_receipt WHERE order_id = '$order_id'";
$query_re = mysqli_query($conn, $sql_re);
while($row_re = mysqli_fetch_array($query_re))
{
  $p_id = $row_re['p_id'];
  
		$sql_p = "SELECT * FROM tbl_products WHERE p_id = '$p_id'";
		$query_p = mysqli_query($conn, $sql_p);
		$row_p = mysqli_fetch_array($query_p);
				
		$sum = $row_p['p_price']*$row_re['cart_num'];
?>
  <tr>
    <td align="center"><?=$no?></td>
    <td align="center"><img src="img_product/<?=$row_p['p_img']?>" width="72px" height="72px"></td>
    <td align="center"><?=$row_p['p_name']?></td>
    <td align="center"><?=number_format($row_p['p_price'],2)?></td>
    <td align="center"><?=$row_re['cart_num']?></td>
    <td align="center"><?=number_format($sum,2)?></td>
  </tr>
<?php
	$no++;
	$allsum = $sum + $allsum;
}
$vat = $allsum * 0.07;
$novat = $allsum - $vat;
$net = $allsum - $data['order_dis'] + $data['cost_ship'];
?>
</table>
<table width="100%" border="0">
  <tr>
    <td align="center">&nbsp;</td>
    <td align="right">&nbsp;</td>
  </tr>
  <tr>
    <td align="center">สถานะ : <?=$data['order_status']?></td>
    <td align="right">&nbsp;</td>
  </tr>
  <tr>
    <td align="center">
    <?php

  $text_ship='';
  $sql_ship = "SELECT * FROM tbl_shipping WHERE order_id = '$order_id'";
	$query_ship = mysqli_query($conn, $sql_ship);
	$row_ship = mysqli_fetch_array($query_ship);
	$num_ship = mysqli_num_rows($query_ship);
    if($num_ship<>0)
	{
		$text_ship="เลขจัดส่งพัสดุ : ".$row_ship['ship_track'];
	}
	?>
    รูปแบบการจัดส่ง : <?=$data['order_ship']?> <?=$text_ship?>
    </td>
    <td align="right">&nbsp;</td>
  </tr>
  <tr>
    <td align="right">ราคาไม่รวมภาษี : </td>
    <td align="right"><?=number_format($novat,2)?> ฿</td>
  </tr>
  <tr>
    <td width="80%" align="right">ภาษีมูลค่าเพิ่ม : </td>
    <td align="right"><?=number_format($vat,2)?> ฿</td>
  </tr>
  <tr>
    <td align="right">ราคารวม : </td>
    <td align="right"><?=number_format($allsum,2)?> ฿</td>
  </tr>
  <tr>
    <td align="right">ส่วนลด : </td>
    <td align="right"><?=number_format($data['order_dis'],2)?> ฿</td>
  </tr>
  <tr>
    <td align="right">ค่าจัดส่ง : </td>
    <td align="right"><?=number_format($data['cost_ship'],2)?> ฿</td>
  </tr>
  <tr>
    <td align="right"><h3>สุทธิ : </h3></td>
    <td align="right"><h3><?=number_format($net,2)?> ฿</h3></td>
  </tr>
  <tr>
    <td align="right">&nbsp;</td>
    <td align="right">&nbsp;</td>
  </tr>
  <tr>
    <td align="right">&nbsp;</td>
    <td align="right">&nbsp;</td>
  </tr>
</table>

</body>
</html>