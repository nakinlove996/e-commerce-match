<h3 style="margin">สินค้าทั้งหมด</h3>

<?php

include('includes/condb.php');

$s = isset($_POST['s'])?$_POST['s']:'';
$c_id = isset($_GET['c_id'])?$_GET['c_id']:'';

if($s <> ''){

    $sql = "SELECT * FROM tbl_products WHERE p_name LIKE '%$s%' OR p_detail LIKE '%$s%' OR p_price LIKE '%$s%' ORDER BY p_id LIMIT 16";
    $query = mysqli_query($conn, $sql);

}elseif($c_id <> ''){

    $sql = "SELECT * FROM tbl_products WHERE c_id LIKE '%$c_id%' ORDER BY p_id LIMIT 16";
    $query = mysqli_query($conn, $sql);

}else{

    $sql = "SELECT * FROM tbl_products ORDER BY p_id LIMIT 16";
    $query = mysqli_query($conn, $sql);

}


while($row = mysqli_fetch_array($query)){ ?>

    <div class="col-sm-3 text-center">
        <div class="goods">
            <div style="margin-bottom:10px;"><a href="product_detail.php?p_id=<?=$row['p_id']?>"><img class="img" src="img_product/<?=$row['p_img']?>" style="width:200px; height:200px;"></a></div>
            <div style="margin-bottom:10px; height:60px;"><a href="product_detail.php?p_id=<?=$row['p_id']?>"><?=$row['p_name']?></a></div>
            <div style="margin-bottom:10px;">ราคา <?=number_format($row['p_price'])?> บาท</div>

            <?php if($row['p_stock'] < 1 ){ ?>
            <div><button class="btn btn-warning disabled">สินค้าหมด</button></div>
            <?php }elseif(isset($_SESSION['id'])){ ?>
            <div><button class="btn btn-success" onclick="window.location.href='add_cart.php?p_id=<?=$row['p_id']?>'">หยิบใส่ตะกร้า</button></div>
            <?php }else{ ?>
            <div><button class="btn btn-success disabled" onclick="alert('กรุณาเข้าสู่ระบบ')">หยิบใส่ตะกร้า</button></div>
            <?php } ?>

        </div>
    </div>

<?php } ?>