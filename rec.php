<h3>สินค้ายอดนิยม</h3>

<?php 

$sql = "SELECT * FROM tbl_products ORDER BY RAND() LIMIT 0,4";
$query = mysqli_query($conn, $sql);

while($row = mysqli_fetch_array($query)){ ?>

<!-- <div class="col-lg-3 col-md-4 col-sm-5 col-xs-8"> -->
<div class="col-md-3 text-center">
    <div class="goods">
        <div style="margin-bottom:10px;"><a href="product_detail.php?p_id=<?=$row['p_id']?>"><img class="img" src="img_product/<?=$row['p_img']?>" style="width:200px; height:200px;"></a></div>
        <div style="margin-bottom:10px; height:60px;"><a href="product_detail.php?p_id=<?=$row['p_id']?>"><?=$row['p_name']?></a></div>
        <div style="margin-bottom:10px;">ราคา <?=number_format($row['p_price'])?> บาท</div>

        <?php if($row['p_stock'] < 1 ){ ?>
        <div><button class="btn btn-warning disabled">สินค้าหมด</button></div>
        <?php }elseif(isset($_SESSION['id'])){ ?>
        <div><button class="btn btn-success" onclick="window.location.href='add_cart.php?p_id=<?=$row['p_id']?>'">หยิบใส่ตะกร้า</button></div>
        <?php }else{ ?>
        <div><button class="btn btn-success disabled" onclick="alert('กรุณาเข้าสู่ระบบ')">หยิบใส่ตะกร้า</button></div>
        <?php } ?>

    </div>
</div>

<?php } ?>