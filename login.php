<div class="modal fade" id="myLogin" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <h3 align="center">Login</h3>
                <form action="code.php" method="post">
                    <div class="form-group">
                        <label class="control-label">Username</label>
                        <input type="text" name="username" class="form-control" placeholder="Username" required>
                    </div>
                    <div class="form-group">
                        <label class="control-label">Password</label>
                        <input type="password" name="password" class="form-control" placeholder="Password" required>
                    </div>
                    <div class="form-group" align="center">
                        <button class="btn btn-success" name="btn_login">ตกลง</button>
                        <button class="btn btn-danger" data-dismiss="modal">ยกเลิก</button>
                    </div>
                    <div align="center">
                        <a data-toggle="modal" data-target="#myRecov" href="#">ลืมรหัส ?</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<?php include('recover.php') ?>