<?php

include('includes/condb.php');

$username = isset($_SESSION['username'])?($_SESSION['username']):'';

$sql = "SELECT * FROM tbl_members WHERE m_username = '$username'";
$query = mysqli_query($conn, $sql);
$row = mysqli_fetch_array($query);

?>
<div class="modal fade" id="myProfile" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <h3 class="form-group" align="center">ข้อมูลผู้ใช้</h3>
                <form action="code.php" method="post" class="form-horizontal">
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Username : </label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" name="username" value="<?=$row['m_username']?>" readonly>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Name : </label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" name="name" value="<?=$row['m_name']?>" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Email : </label>
                        <div class="col-sm-8">
                            <input type="email" class="form-control" name="email" value="<?=$row['m_email']?>" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Telephone : </label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" name="telephone" value="<?=$row['m_telephone']?>" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Address : </label>
                        <div class="col-sm-8">
                            <textarea name="address" id="" cols="30" rows="4" class="form-control"><?=$row['m_address']?></textarea>
                        </div>
                    </div>
                    <div class="form-group" align="center">
                        <button class="btn btn-success" name="btn_edit">ตกลง</button>
                        <button class="btn btn-danger" data-dismiss="modal">ยกเลิก</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>