
<?php
if($_SESSION['role'] !== 'admin'){
    header('location: ../index.php');
}else{
    ?>
        <form action="" method="post">
            <h1 align="center">ระบบจัดการหลังร้าน</h1>
            <hr>
            <div class="panel panel-default">
                <div class="panel-heading">ระบบจัดการสมาชิก</div>
                <div class="panel panel-body">
                    <a href="index.php?link=user">เพิ่ม ลบ แก้ไข แสดงรายชื่อสมาชิก</a>
                </div>
            </div>

            <div class="panel panel-default">
                <div class="panel-heading">ระบบจัดการสินค้า</div>
                <div class="panel panel-body">
                    <a href="index.php?link=cat">เพิ่ม ลบ แก้ไข แสดงรายชื่อหมวดหมู่สินค้า</a><br>
                    <a href="index.php?link=p">เพิ่ม ลบ แก้ไข แสดงรายการสินค้า</a>
                </div>
            </div>

            <div class="panel panel-default">
                <div class="panel-heading">ระบบจัดการโปรโมชั่น</div>
                <div class="panel panel-body">
                    <a href="index.php?link=pro">เพิ่ม ลบ แสดงรายการโปรโมชั่น</a>
                </div>
            </div>

            <div class="panel panel-default">
                <div class="panel-heading">ระบบจัดการสมาชิก</div>
                <div class="panel panel-body">
                <a href="index.php?link=view">แสดงการสั่งซื้อสินค้า</a><br>
                <a href="index.php?link=order">แจ้งชำระ ปรับสถานะ แจ้งเลขพัสดุ</a><br>
                <a href="index.php?link=report">รายงานยอดการขาย</a>
                </div>
            </div>
        </form>  
    <?php
}
?>