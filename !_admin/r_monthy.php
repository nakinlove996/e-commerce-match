
            <?php
            $query = "SELECT order_net, SUM(order_net) AS totol, DATE_FORMAT(day_pay, '%M-%Y') AS day_pay
            FROM tbl_payment
            GROUP BY DATE_FORMAT(day_pay, '%m%')
            ORDER BY DATE_FORMAT(day_pay, '%Y-%m-%d') DESC 
            ";
            $result = mysqli_query($conn, $query);
           
?>
    <a href="index.php?link=report" class="btn btn-danger"><span class="glyphicon glyphicon-arrow-left"></span> กลับ</a>
                <h3>ยอดการขายแต่ละเดือน</h3>
                <table  class="table table-striped" border="1" cellpadding="0"  cellspacing="0" align="center">
                    <thead>
                        <tr class="info">
                            <th width="30%"><h4>ด/ป</h4></th>
                            <th width="70%"><center><h4>รายได้</h4></center></th>
                        </tr>
                    </thead>
                    
                    <?php while($row = mysqli_fetch_array($result)) { ?>
                    <tr>
                        <td> -<?php echo $row['day_pay'];?></td>
                        <td align="right"><?php echo number_format($row['totol'],2);?></td>
                    </tr>
                    <?php
                    @$amount_total += $row['totol'];
                    }
                    ?>
                    <tr class="danger">
                        <td align="center">รวม</td>
                        <td align="right"><b>
                        <?php echo number_format($amount_total,2);?></b></td></td>
                    </tr>
                </table>
               
            </div>
    </div>
</div>