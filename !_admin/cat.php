
<?php
if($_SESSION['role'] !== 'admin'){
    header('location: ../index.php');
}else{
   
    ?>
        <form action="" method="post" class="form-inline">
            <h1 align="center">ระบบจัดการหมวดหมู่สินค้า</h1>
            <hr>
            <div class="form-group">
                <a href="index.php?link=addcat" class="btn btn-info">+เพิ่มหมวดหมู่สินค้า</a>
            </div>
            <button class="btn btn-warning" name="hide"><span class="glyphicon glyphicon-eye-close"></span> แสดงหมวดหมู่ที่ยกเลิก</button>

            <div class="input-group">
                <input type="search" name="sc" class="form-control">
                    <div class="input-group-btn">
                        <button class="btn btn-success"><span class="glyphicon glyphicon-search"></span></button>
                    </div>
            </div>

            <br>
            <br>
            <?php
                if(isset($_SESSION['status']) && $_SESSION['status']){
                    echo '<div class="alert alert-success" role="alert">
                        <h3 align="center">'.$_SESSION['status'].'</h3></div>';
                    unset($_SESSION['status']);
                }
                if(isset($_SESSION['status_error']) && $_SESSION['status_error']){
                    echo '<div class="alert alert-danger" role="alert"><h3 align="center">'.$_SESSION['status_error'].'</h3></div>';
                    unset($_SESSION['status_error']);
                }
            ?>
            <div class="table-reponsive">
                <table class="table table-hover table-bordered">
                    <thead>
                        <th style="text-align:center"><h4>#</h4></th>
                        <th style="text-align:center"><h4>ชื่อหมวดหมู่</h4></th>
                        <th style="text-align:center"><h4>สถานะ</h4></th>
                        <th style="text-align:center"><h4>เพิ่มเติม</h4></th>
                    </thead>
                    <?php
                        $s = isset($_POST['ssssssc']) ? $_POST['sc']:'';
                        if($s!=''){
                            $sql = "SELECT * FROM tbl_category WHERE c_id LIKE '%$s%' OR c_name LIKE '%$s%' ";
                            $result = mysqli_query($conn,$sql);
                        
                        }elseif(isset($_POST['hide'])){
                            $sql = "SELECT * FROM tbl_category WHERE c_status='ยกเลิก'";
                            $result = mysqli_query($conn,$sql);
                        }else{
                            $perpage = 12;
                            if (isset($_GET['page'])) {
                            $page = $_GET['page'];
                            } else {
                            $page = 1;
                            }
                            $start = ($page - 1) * $perpage;
                            //  echo $start;
                            $query = "SELECT * FROM tbl_category  limit $start, $perpage";
                            $result = mysqli_query($conn,$query);
                            $query2 = mysqli_query($conn, $query);
                            $total_record = mysqli_num_rows($query2);
                            $total_page = ceil($total_record / $perpage);
                            // echo $total_record;
                            // echo $total_page ;
                        }while($row = mysqli_fetch_array($result)){
                            $m_id =$row['m_id'];
                            $sql1 = "SELECT * FROM tbl_members WHERE m_id='$m_id'";
                            $result1 = mysqli_query($conn,$sql1);
                            $ad =mysqli_fetch_array($result1);

                            ?>
                                <tbody  align="center">
                                    <th width="5%" style="text-align:center"><?=$row['c_id']?></th>
                                    <td><?=$row['c_name']?></td>
                                    </form>
                                  
                                        <td width="28%">
                                        <form action="" method="post">
                                        <input type="hidden" value="<?=$row['c_id']?>" name="c_id">
                                        <select name="c_status" class="form-control" required onchange="this.form.submit();">
                                            <option value="<?=$row['c_status']?>">สถานะ : <?=$row['c_status']?></option>
                                            <option >เปิดใช้งาน</option>
                                            <option >ยกเลิก</option>
                                        </select>
                                       <?php
                                        if(isset($_POST['c_status'])){
                                            $c_id = $_POST['c_id'];
                                            $c_status = $_POST['c_status'];
                                            $sql = "UPDATE tbl_category SET c_status='$c_status' WHERE c_id='$c_id'";
                                            $result = mysqli_query($conn,$sql);
                                            if($result){
                                                $_SESSION['status'] = 'ยกเลิกสำเร็จ';
                                                ?>
                                                <script>
                                                    window.location.href="index.php?link=cat"
                                                </script>
                                                <?php
                                            }else{
                                                $_SESSION['status_error'] = 'ยกเลิกไม่สำเร็จ';
                                                ?>
                                                <script>
                                                    window.location.href="index.php?link=cat"
                                                </script>
                                                <?php
                                            }
                                        }
                                       ?>
                                        </td>
                                    </form>
                                   
                                    <td width="20%" >
                                    <form action="" method="post" class="form-inline">
                                        <a href="index.php?link=editcat&ID=<?=$row['c_id']?>" class="btn btn-warning"><span class="glyphicon glyphicon-pencil"></span></a>
                                            <input type="hidden" name="c_id" value="<?=$row['c_id']?>">
                                            <button class="btn btn-danger" name="del_cat" onclick="return confirm('ลบข้อมูล จะไม่สามารถกู้คืนได้ ?')"><span class="glyphicon glyphicon-trash"></span></button>
                                            <?php
                                                if(isset($_POST['del_cat'])){
                                                    $c_id =$_POST['c_id'];
                                                    $sql ="DELETE FROM tbl_category WHERE c_id='$c_id'";
                                                    $result=mysqli_query($conn,$sql);
                                                    if($result){
                                                        $_SESSION['status'] = 'ลบสำเร็จ';
                                                        ?>
                                                        <script>
                                                            window.location.href="index.php?link=cat"
                                                        </script>
                                                        <?php
                                                    }else{
                                                        $_SESSION['status_error'] = 'ลบไม่สำเร็จ';
                                                        ?>
                                                        <script>
                                                            window.location.href="index.php?link=cat"
                                                        </script>
                                                        <?php
                                                    }
                                                }
                                            ?>
                                        </form>
                                        <small class="pull-right">เปลี่ยนแปลงโดย : คุณ <d style="color:green;"><?=$ad['m_name']?></d></small>
                                    </td>
                                    
                                </tbody>
                             
                            <?php
                        }
                    ?>
                    
                </table>
                <center>
               
                        <nav>
                            <ul class="pagination">
                              <li>
                                <a href="index.php?link=cat&page=1" aria-label="Previous">
                                  <span aria-hidden="true">&laquo;</span>
                                    </a>
                                      </li>
                                        <?php for($i=1;$i<=$total_page;$i++){ ?>
                                        <li><a href="index.php?link=cat&page=<?php echo $i; ?>"><?php echo $i; ?></a></li>
                                        <?php } ?>
                                      <li>
                                    <a href="index.php?link=cat&page=<?php echo $total_page;?>" aria-label="Next">
                                  <span aria-hidden="true">&raquo;</span>
                                </a>
                              </li>
                            </ul>
                          </nav>
                          <!--next page-->
                </center>
            </div>
          
    <?php
}
?>