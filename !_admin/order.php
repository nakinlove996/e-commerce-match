
<?php
if($_SESSION['role'] !== 'admin'){
    header('location: ../index.php');
}else{
   
    ?>
        <form action="" method="post" class="form-inline">
            <h1 align="center">ระบบจัดการสถานะการสั่งซื้อ</h1>
            <hr>
            <select name="sv" class="form-control" >
                <option value="">แสดงทั้งหมด</option>
                <option>ชำระเรียบร้อย/รอตรวจสอบ</option>
                <option>ตรวจสอบเรียบร้อย/รอจัดส่ง</option>
                <option>จัดส่งสินค้าเรียบร้อย</option>
                <option>ยกเลิก</option>
            </select>
            <button class="btn btn-success">ค้นหา</button>
            <br>
            <br>
            <?php
                if(isset($_SESSION['status']) && $_SESSION['status']){
                    echo '<h2 style="color:green;">'.$_SESSION['status'].'</h2>';
                    unset($_SESSION['status']);
                }
                if(isset($_SESSION['status_error']) && $_SESSION['status_error']){
                    echo '<h2 style="color:red;">'.$_SESSION['status_error'].'</h2>';
                    unset($_SESSION['status_error']);
                }
            ?>
            <div class="table-reponsive">
                <table class="table table-hover table-bordered">
                    <thead>
                        <th style="text-align:center"><h4>เลขใบสั่งซื้อ</h4></th>
                        <th style="text-align:center"><h4>รูปแบบการจัดส่ง</h4></th>
                        <th style="text-align:center"><h4>วันที่สั่งซื้อ</h4></th>
                        <th style="text-align:center"><h4>วันที่ชำระ</h4></th>
                        <th style="text-align:center"><h4>ราคาสินค้า</h4></th>
                        <th style="text-align:center"><h4>สถานะสินค้า</h4></th>
                        <th style="text-align:center"><h4>เพิ่มเติม</h4></th>
                        <th></th>
                    </thead>
                    <?php
                        $s = isset($_POST['sv']) ? $_POST['sv']:'';
                        if($s!=''){
                            $sql = "SELECT * FROM tbl_order WHERE order_status LIKE '%$s%' ";
                            $result = mysqli_query($conn,$sql);
                        }else{
                            $sql = "SELECT * FROM tbl_order ";
                            $result = mysqli_query($conn,$sql);

                        }while($row = mysqli_fetch_array($result)){
                            $order_id = $row['order_id'];
                            $sql1 = "SELECT * FROM tbl_payment WHERE order_id ='$order_id'";
                            $result1 = mysqli_query($conn,$sql1);
                            $data = mysqli_fetch_array($result1);

                            $m_id = $row['a_id'];
                            $sql2 = "SELECT * FROM tbl_members WHERE m_id ='$m_id'";
                            $result2 = mysqli_query($conn,$sql2);
                            $data1 = mysqli_fetch_array($result2);
                            ?>
                            <?php
                                if($data['day_pay'] == NULL ){
                                    
                                }elseif($row['order_status'] == 'จัดส่งสินค้าเรียบร้อย'){
                                    
                                    ?>
                                    <tbody align="center" >
                                    <th style="text-align:center"><?=$row['order_id']?></th>
                                    <td><?=$row['order_ship']?></td>
                                    <td><?=$row['order_date']?></td>
                                    <td><?=$data['day_pay']?></td>
                                    <td><?=number_format($row['order_net'],2)?></td>
                                    </form>
                                    <td>
                                           <p style="color:green;">จัดส่งสินค้าเรียบร้อยไม่สามารถแก้ไขได้</p> 
                                        <small class="pull-right">เปลี่ยนแปลงล่าสุดโดย : คุณ <d style="color:green;"><?=$data1['m_name']?></d></small>
                                    <?php
                                }else{
                                    ?>
                                     <tbody align="center" >
                                    <th style="text-align:center"><?=$row['order_id']?></th>
                                    <td><?=$row['order_ship']?></td>
                                    <td><?=$row['order_date']?></td>
                                    <td><?=$data['day_pay']?></td>
                                    <td><?=number_format($row['order_net'],2)?></td>
                                    </form>
                                    <td>
                                    <form action="" method="post" class="form-inline">
                                    <input type="hidden" name="order_id" value="<?=$row['order_id']?>">
                                        <select name="order_status" class="form-control" required onchange="this.form.submit();">
                                            <option value="<?=$row['order_status']?>">สถานะการสั่งซื้อ : <?=$row['order_status']?></option>
                                            <option >ตรวจสอบเรียบร้อย/รอจัดส่ง</option>
                                            <option >ยกเลิก</option>
                                        </select>

                                        <?php
                                            if(isset($_POST['order_status'])){
                                                $order_id =$_POST['order_id'];
                                                $sm_id =$_SESSION['id'];
                                                $order_status = $_POST['order_status'];

                                                 $sql = "UPDATE tbl_order SET a_id='$sm_id',order_status='$order_status' WHERE order_id ='$order_id'";
                                                 $result = mysqli_query($conn,$sql);
                                                 if($result){
                                                    $_SESSION['status'] = 'แก้ไขสำเร็จ';
                                                    ?>
                                                    <script>
                                                        window.location.href="index.php?link=order"
                                                    </script>
                                                    <?php
                                                }else{
                                                    $_SESSION['status_error'] = 'แก้ไขไม่สำเร็จ';
                                                    ?>
                                                    <script>
                                                        window.location.href="index.php?link=order"
                                                    </script>
                                                    <?php
                                                }
                                            }
                                        ?>
                                        <br>

                                        <small class="pull-right">เปลี่ยนแปลงล่าสุดโดย : คุณ <d style="color:green;"><?=$data1['m_name']?></d></small>
                                        </form>
                                    </td>
                                    <?php
                                   }
                                   ?>
                                    <td>
                                    <?php
                                        if($row['order_status']=='ยกเลิก'){
                                           ?>
                                            <button onclick="window.open('slip_pay.php?id_pay=<?=$data['id_pay']?>','_popup','width=350px,height=500px');return false" class="btn btn-success"><span class="glyphicon glyphicon-eye-open"></span> ข้อมูลการโอนเงิน</button>
                                           <?php
                                        }elseif($row['order_status']=='ชำระเงินเรียบร้อย/รอตรวจสอบ'){
                                            ?>
                                            <button onclick="window.open('slip_pay.php?id_pay=<?=$data['id_pay']?>','_popup','width=350px,height=500px');return false" class="btn btn-success"><span class="glyphicon glyphicon-eye-open"></span> ข้อมูลการโอนเงิน</button>
                                           <?php
                                        }elseif($row['order_status']=='ตรวจสอบเรียบร้อย/รอจัดส่ง'){
                                            ?>
                                            <button onclick="window.open('slip_pay.php?id_pay=<?=$data['id_pay']?>','_popup','width=350px,height=500px');return false" class="btn btn-success"><span class="glyphicon glyphicon-eye-open"></span> ข้อมูลการโอนเงิน</button>
                                           <?php
                                        }elseif($row['order_status']=='จัดส่งสินค้าเรียบร้อย'){
                                            ?>
                                            <button onclick="window.open('slip_pay.php?id_pay=<?=$data['id_pay']?>','_popup','width=350px,height=500px');return false" class="btn btn-success"><span class="glyphicon glyphicon-eye-open"></span> ข้อมูลการโอนเงิน</button>
                                           <?php
                                        }
                                        ?>
                                    </td>
                                    <td><?php
                                        if($row['order_status']=='ยกเลิก'){
                                           ?>
                                           <input type="hidden" name="id_pay" value="<?=$data['id_pay']?>">
                                           <input type="hidden" name="order_id" value="<?=$row['order_id']?>">
                                            <button class="btn btn-danger" name="del_p_o" onclick="return confirm('ลบข้อมูล ?')"><span class="glyphicon glyphicon-trash"></span></button>
                                            <?php
                                                if(isset($_POST['del_p_o'])){
                                                    $id_pay =$_POST['id_pay'];
                                                    $order_id =$_POST['order_id'];
                                                    $sql ="DELETE FROM tbl_order WHERE order_id='$order_id'";
                                                    $result=mysqli_query($conn,$sql);
                                                    $sql ="DELETE FROM tbl_payment WHERE id_pay='$id_pay'";
                                                    $result=mysqli_query($conn,$sql);
                                                    if($result){
                                                        $_SESSION['status'] = 'ลบสำเร็จ';
                                                        ?>
                                                        <script>
                                                            window.location.href="index.php?link=order"
                                                        </script>
                                                        <?php
                                                    }else{
                                                        $_SESSION['status_error'] = 'ลบไม่สำเร็จ';
                                                        ?>
                                                        <script>
                                                            window.location.href="index.php?link=order"
                                                        </script>
                                                        <?php
                                                    }
                                                }
                                            ?>
                                           <?php 
                                        }elseif($row['order_status']=='ตรวจสอบเรียบร้อย/รอจัดส่ง'){
                                            ?>
                                             
                                                <a href="shipping.php?order_id=<?=$row['order_id']?>" class="btn btn-danger"><span class="glyphicon glyphicon-plus"> เลขพัสดุ</span></a>
                                            <?php
                                        }elseif($row['order_status']=='จัดส่งสินค้าเรียบร้อย') {
                                            ?>
                                             
                                                <a href="edit_ship.php?order_id=<?=$row['order_id']?>" class="btn btn-warning"><span class="glyphicon glyphicon-pencil"> แก้ไขเลขพัสดุ</span></a>
                                            <?php
                                        }
                                    ?></td>
                                </tbody>
                                    
                                
                            <?php
                        }
                    ?>
                </table>
              
            </div>
          
    <?php
}
?>