
<?php
if($_SESSION['role'] !== 'admin'){
    header('location: ../index.php');
}else{
   
    ?>
        <form action="" method="post" class="form-inline">
            <h1 align="center">ระบบจัดการโปรโมชั่น</h1>
            <hr>
            <div class="form-group">
                <a href="index.php?link=addpro" class="btn btn-info">+เพิ่มโปรโมชั่น</a>
            </div>

            <div class="input-group">
                <input type="search" name="spro" class="form-control">
                    <div class="input-group-btn">
                        <button class="btn btn-success"><span class="glyphicon glyphicon-search"></span></button>
                    </div>
            </div>
            <br>
            <br>
            <?php
                if(isset($_SESSION['status']) && $_SESSION['status']){
                    echo '<h2 style="color:green;">'.$_SESSION['status'].'</h2>';
                    unset($_SESSION['status']);
                }
                if(isset($_SESSION['status_error']) && $_SESSION['status_error']){
                    echo '<h2 style="color:red;">'.$_SESSION['status_error'].'</h2>';
                    unset($_SESSION['status_error']);
                }
            ?>
            <div class="table-reponsive">
                <table class="table table-hover table-bordered">
                    <thead>
                        <th style="text-align:center"><h4>#</h4></th>
                        <th style="text-align:center"><h4>โค้ดโปรโมชั่น</h4></th>
                        <th style="text-align:center"><h4>รายละเอียดโปรโมชั่น</h4></th>
                        <th style="text-align:center"><h4>ส่วนลด</h4></th>
                        <th style="text-align:center"><h4>ประเภท</h4></th>
                        <th style="text-align:center"><h4>ขั้นต่ำในการสั่งซื้อ</h4></th>
                        <th style="text-align:center"><h4>สภานะ</h4></th>
                        <th ></th>
                    </thead>
                    <?php
                        $s = isset($_POST['spro']) ? $_POST['spro']:'';
                        if($s!=''){
                            $sql = "SELECT * FROM tbl_promotion WHERE pro_id LIKE '%$s%' OR pro_code LIKE '%$s%' OR pro_detail LIKE '%$s%' OR pro_dis LIKE '%$s%' OR pro_type LIKE '%$s%' OR pro_min LIKE '%$s%' OR pro_status LIKE '%$s%'";
                            $result = mysqli_query($conn,$sql);
                        }else{
                            $sql = "SELECT * FROM tbl_promotion ";
                            $result = mysqli_query($conn,$sql);
                        }while($row = mysqli_fetch_array($result)){
                            $m_id =$row['m_id'];
                            $sql1 ="SELECT * FROM tbl_members WHERE m_id='$m_id'";
                            $result1 = mysqli_query($conn,$sql1);
                            $data = mysqli_fetch_array($result1);

                            ?>
                                <tbody align="center" >
                                    <th style="text-align:center"><?=$row['pro_id']?></th>
                                    <td><?=$row['pro_code']?></td>
                                    <td><?=$row['pro_detail']?></td>
                                    <td><?=number_format($row['pro_dis'],2)?></td>
                                    <td><?=$row['pro_type']?></td>
                                    <td><?=number_format($row['pro_min'],2)?></td>
                                    </form>
                                    <td width="25%">
                                        <form action="" method="post">
                                            <input type="hidden" name="pro_id" value="<?=$row['pro_id']?>">
                                            <select name="pro_status" class="form-control" onchange="this.form.submit();">
                                                <option value="<?=$row['pro_status']?>">สภานะ : <?=$row['pro_status']?></option>
                                                <option >เปิดใช้งาน</option>
                                                <option >ปิดใช้งาน</option>
                                            </select><br>
                                            <small class="pull-right">เปลี่ยนแปลงครั้งล่าสุดโดย : <d style="color:green;"><?=$data['m_name']?></small>
                                            <?php
                                                if(isset($_POST['pro_status'])){
                                                   $pro_id =$_POST['pro_id'];
                                                   $sm_id = $_SESSION['id'];
                                                   $pro_status = $_POST['pro_status'];
                                                   $sql = "UPDATE tbl_promotion SET m_id='$sm_id',pro_status='$pro_status' WHERE pro_id='$pro_id'";
                                                   $result = mysqli_query($conn,$sql);
                                                   if($result){
                                                    $_SESSION['status'] = 'แก้ไขสำเร็จ';
                                                    ?>
                                                    <script>
                                                        window.location.href="index.php?link=pro"
                                                    </script>
                                                    <?php
                                                }else{
                                                    $_SESSION['status_error'] = 'แก้ไขไม่สำเร็จ';
                                                    ?>
                                                    <script>
                                                        window.location.href="index.php?link=pro"
                                                    </script>
                                                    <?php
                                                }
                                                }
                                            ?>
                                        </form>
                                    </td>
                                    <td>
                                    <?php
                                        if($row['pro_status']!=='เปิดใช้งาน'){
                                            ?>
                                                <form action="" method="post" class="form-inline">
                                                        <input type="hidden" name="pro_id" value="<?=$row['pro_id']?>">
                                                        <button class="btn btn-danger" name="del_pro" onclick="return confirm('ลบข้อมูล ?')"><span class="glyphicon glyphicon-trash"></span></button>
                                                        <?php
                                                            if(isset($_POST['del_pro'])){
                                                                $pro_id =$_POST['pro_id'];
                                                                $sql ="DELETE FROM tbl_promotion WHERE pro_id='$pro_id'";
                                                                $result=mysqli_query($conn,$sql);
                                                                if($result){
                                                                    $_SESSION['status'] = 'ลบสำเร็จ';
                                                                    ?>
                                                                    <script>
                                                                        window.location.href="index.php?link=pro"
                                                                    </script>
                                                                    <?php
                                                                }else{
                                                                    $_SESSION['status_error'] = 'ลบไม่สำเร็จ';
                                                                    ?>
                                                                    <script>
                                                                        window.location.href="index.php?link=pro"
                                                                    </script>
                                                                    <?php
                                                                }
                                                            }
                                                        ?>
                                                </form>
                                            <?php
                                        }
                                    ?>
                                    </td>
                                </tbody>
                            <?php
                        }
                    ?>
                </table>
            
            </div>
          
    <?php
}
?>