<?php
if($_SESSION['role'] !== 'admin'){
    header('location: ../index.php');
}else{
    $m_id = $_REQUEST['ID'];
    $sql = "SELECT * FROM tbl_members WHERE m_id='$m_id'";
    $result = mysqli_query($conn,$sql);
    $data = mysqli_fetch_array($result);
    
    ?>
        <form action="" method="post" >
            <h1 >แก้ไขสมาชิก</h1>
            <hr>
            <div class="form-group">
                <label>ชือ-สกุล</label>
                <input type="text" name="m_name" class="form-control" placeholder="กรอกชื่อ-สกุล" required value="<?=$data['m_name']?>">
            </div>
            <div class="form-group">
                <label>เบอร์โทร</label>
                <input type="text" name="m_telephone" class="form-control" placeholder="กรอกเบอร์โทร" required value="<?=$data['m_telephone']?>" maxlength="10">
            </div>
            <div class="form-group">
                <label>อีเมล</label>
                <input type="text" name="m_email" class="form-control" placeholder="กรอกอีเมล" required value="<?=$data['m_email']?>">
            </div>
            <div class="form-group">
                <label>ที่อยู่</label>
                <textarea name="m_address" rows="5" required class="form-control" placeholder="กรอกที่อยู่"><?=$data['m_address']?></textarea>
            </div>
            <hr>
            <div class="form-group">
                <label>ชื่อผู้ใช้</label>
                <input type="text" name="m_username" class="form-control" placeholder="กรอกชื่อผู้ใช้" required value="<?=$data['m_username']?>">
            </div>
            <div class="form-group">
                <label>รหัสผ่าน</label>
                <input type="text" name="m_password" class="form-control" placeholder="กรอกรหัสผ่าน" required value="<?=$data['m_password']?>">
            </div>
            <div class="form-group">
                <label>คำถามกู้คืนรหัส</label>
                <select name="m_q_pass" class="form-control" required>
                    <option value="<?=$data['m_q_pass']?>">คำถามกู้คืนรหัส : <?=$data['m_q_pass']?></option>
                    <option >สีที่ชอบ</option>
                   <option >เลขที่ชอบ</option>
                   <option >อาหารโปรด</option>
                </select>
            </div>
            <div class="form-group">
                <label>คำตอบกู้คืนรหัส</label>
                <input type="text" name="m_a_pass" class="form-control" placeholder="กรอกคำตอบกู้คืนรหัส" required value="<?=$data['m_a_pass']?>">
            </div>
            <div class="form-group">
                <label>ตำแหน่ง</label>
                <select name="m_role" class="form-control" required >
                    <option value="<?=$data['m_role']?>">ตำแหน่ง : <?=$data['m_role']?></option>
                    <option >admin</option>
                   <option >member</option>
                </select>
            </div>
            <div class="form-group">
            <input type="hidden" value="<?=$data['m_id']?>" name="m_id">
                <button class="btn btn-success" name="edituser"><span class="glyphicon glyphicon-ok"></span></button>
                <a href="index.php?link=user" class="btn btn-danger"><span class="glyphicon glyphicon-remove"></span></a>
            </div>
            <?php
                if(isset($_POST['edituser'])){
                    $m_id=$_POST['m_id'];

                    $m_name=$_POST['m_name'];
                    $m_telephone=$_POST['m_telephone'];
                    $m_email=$_POST['m_email'];
                    $m_address=$_POST['m_address'];
                    $m_username=$_POST['m_username'];
                    $m_password=$_POST['m_password'];
                    $m_q_pass=$_POST['m_q_pass'];
                    $m_a_pass=$_POST['m_a_pass'];
                    $m_role=$_POST['m_role'];

                        $sql = "UPDATE tbl_members SET m_name='$m_name',m_telephone='$m_telephone',m_email='$m_email',m_address='$m_address',m_username='$m_username',m_password='$m_password',m_q_pass='$m_q_pass',m_a_pass='$m_a_pass',m_role='$m_role' WHERE m_id='$m_id'";
                        $result = mysqli_query($conn,$sql);
                        if($result){
                            $_SESSION['status'] = 'แก้ไขข้อมูลสำเร็จ';
                            ?>
                             <script>
                                window.location.href="index.php?link=user"
                             </script>
                            <?php
                        }else{
                            $_SESSION['status_error'] = 'แก้ไขไม่ข้อมูลสำเร็จ';
                            ?>
                             <script>
                                window.location.href="index.php?link=user"
                             </script>
                            <?php
                        }
                    
                }
            ?>
        </form>
    <?php
}
?>