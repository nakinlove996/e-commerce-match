
<?php
if($_SESSION['role'] !== 'admin'){
    header('location: ../index.php');
}else{
    ?>
        <form action="" method="post" >
            <h1 >เพิ่มสมาชิก</h1>
            <hr>
            <div class="form-group">
                <label>ชือ-สกุล</label>
                <input type="text" name="m_name" class="form-control" placeholder="กรอกชื่อ-สกุล" required>
            </div>
            <div class="form-group">
                <label>เบอร์โทร</label>
                <input type="text" name="m_telephone" class="form-control" placeholder="กรอกเบอร์โทร" required maxlength="10">
            </div>
            <div class="form-group">
                <label>อีเมล</label>
                <input type="email" name="m_email" class="form-control" placeholder="กรอกอีเมล" required>
            </div>
            <div class="form-group">
                <label>ที่อยู่</label>
                <textarea name="m_address" rows="5" required class="form-control" placeholder="กรอกที่อยู่"></textarea>
            </div>
            <hr>
            <div class="form-group">
                <label>ชื่อผู้ใช้</label>
                <input type="text" name="m_username" class="form-control" placeholder="กรอกชื่อผู้ใช้" required>
            </div>
            <div class="form-group">
                <label>รหัสผ่าน</label>
                <input type="text" name="m_password" class="form-control" placeholder="กรอกรหัสผ่าน" required>
            </div>
            <div class="form-group">
                <label>คำถามกู้คืนรหัส</label>
                <select name="m_q_pass" class="form-control" required>
                    <option value="">กรุณาเลือกคำถามกู้คืนรหัส</option>
                    <option >สีที่ชอบ</option>
                   <option >เลขที่ชอบ</option>
                   <option >อาหารโปรด</option>
                </select>
            </div>
            <div class="form-group">
                <label>คำตอบกู้คืนรหัส</label>
                <input type="text" name="m_a_pass" class="form-control" placeholder="กรอกคำตอบกู้คืนรหัส" required>
            </div>
            <div class="form-group">
                <label>ตำแหน่ง</label>
                <select name="m_role" class="form-control" required>
                    <option value="">กรุณาเลือกตำแหน่ง</option>
                    <option >admin</option>
                   <option >member</option>
                </select>
            </div>
            <div class="form-group">
                <button class="btn btn-success" name="adduser"><span class="glyphicon glyphicon-ok"></span></button>
                <a href="index.php?link=user" class="btn btn-danger"><span class="glyphicon glyphicon-remove"></span></a>
            </div>
            <?php
                if(isset($_POST['adduser'])){
                    $m_name=$_POST['m_name'];
                    $m_telephone=$_POST['m_telephone'];
                    $m_email=$_POST['m_email'];
                    $m_address=$_POST['m_address'];
                    $m_username=$_POST['m_username'];
                    $m_password=$_POST['m_password'];
                    $m_q_pass=$_POST['m_q_pass'];
                    $m_a_pass=$_POST['m_a_pass'];
                    $m_role=$_POST['m_role'];
                    $sql_c = "SELECT * FROM tbl_members WHERE m_username='$m_username' AND m_emil='$m_email'";
                    $result_c =mysqli_query($conn,$sql_c);
                    if(mysqli_num_rows($result_c) > 0){
                            $_SESSION['status_error'] ='ชื่อผู้ใช้และอีเมล์นี้มีในระบบแล้ว';
                            ?>
                            <script>
                                window.location.href="index.php?link=user"
                            </script>
                            <?php
                    }else{
                        $sql = "INSERT INTO tbl_members(m_name,m_telephone,m_email,m_address,m_username,m_password,m_q_pass,m_a_pass,m_role) VALUES ('$m_name','$m_telephone','$m_email','$m_address','$m_username','$m_password','$m_q_pass','$m_a_pass','$m_role')";
                        $result = mysqli_query($conn,$sql);
                        if($result){
                            $_SESSION['status'] = 'เพิ่มข้อมูลสำเร็จ';
                            ?>
                             <script>
                                window.location.href="index.php?link=user"
                             </script>
                            <?php
                        }else{
                            $_SESSION['status_error'] = 'เพิ่มไม่ข้อมูลสำเร็จ';
                            ?>
                             <script>
                                window.location.href="index.php?link=user"
                             </script>
                            <?php
                        }
                    }
                }
            ?>
        </form>
    <?php
}
?>