
<?php
if($_SESSION['role'] !== 'admin'){
    header('location: ../index.php');
}else{
    $c_id = $_REQUEST['ID'];
    $sql = "SELECT * FROM tbl_category WHERE c_id='$c_id'";
    $result = mysqli_query($conn,$sql);
    $data = mysqli_fetch_array($result);
    ?>
        <form action="" method="post" >
            <h1 >แก้ไขชือหมวดหมู่สินค้า</h1>
            <hr>
            <div class="form-group">
                <label>ชือหมวดหมู่</label>
                <input type="text" name="c_name" class="form-control" placeholder="กรอกชือหมวดหมู่" required value=<?=$data['c_name']?>>
                <input type="hidden" name="c_status" class="form-control"  value="เปิดใช้งาน">
            </div>
            <div class="form-group">
            <input type="hidden" value="<?=$data['c_id']?>" name="c_id">
                <button class="btn btn-success" name="editcat"><span class="glyphicon glyphicon-ok"></span></button>
                <a href="index.php?link=cat" class="btn btn-danger"><span class="glyphicon glyphicon-remove"></span></a>
            </div>
            <?php
                if(isset($_POST['editcat'])){
                    $c_id=$_POST['c_id'];
                    $c_name=$_POST['c_name'];
                    $sm_id =$_SESSION['id'];
                    $sql_c = "SELECT * FROM tbl_category WHERE c_name='$c_name' ";
                    $result_c =mysqli_query($conn,$sql_c);
                    if(mysqli_num_rows($result_c) > 0){
                            $_SESSION['status_error'] ='ชื่อหมวดหมู่นี้มีในระบบแล้ว';
                            ?>
                            <script>
                                window.location.href="index.php?link=cat"
                            </script>
                            <?php
                    }else{
                        $sql = "UPDATE tbl_category SET c_name='$c_name',m_id='$sm_id' WHERE c_id='$c_id'";
                        $result = mysqli_query($conn,$sql);
                        if($result){
                            $_SESSION['status'] = 'แก้ไขข้อมูลสำเร็จ';
                            ?>
                             <script>
                                window.location.href="index.php?link=cat"
                             </script>
                            <?php
                        }else{
                            $_SESSION['status_error'] = 'แก้ไขไม่ข้อมูลสำเร็จ';
                            ?>
                             <script>
                                window.location.href="index.php?link=cat"
                             </script>
                            <?php
                        }
                    }
                }
            ?>
        </form>
    <?php
}
?>