
<?php
if($_SESSION['role'] !== 'admin'){
    header('location: ../index.php');
}else{
    ?>
        <form action="" method="post" >
            <h1 >เพิ่มโค้ดโปรโมชั่น</h1>
            <hr>
            <div class="form-group">
                <label>โด้ดโปรโมชั่น</label>
                <input type="text" name="pro_code" class="form-control" placeholder="กรอกโด้ดโปรโมชั่น" required>
            </div>
            <div class="form-group">
                <label>รายละเอียดโปรโมชั่น</label>
                <textarea name="pro_detail" rows="5" required class="form-control" placeholder="กรอกรายละเอียดโปรโมชั่น"></textarea>
            </div>
            <div class="form-group">
                <label>ส่วนลด</label>
                <input type="number" min="1" name="pro_dis" class="form-control" placeholder="กรอกส่วนลด" required>
            </div>
            <div class="form-group">
                <label>ประเภท</label>
                <select name="pro_type" class="form-control"  required>
                    <option value="">กรุณาเลือกประเภทส่วนลด</option>
                    <option >บาท</option>
                    <option >เปอร์เซ็น</option>
                </select>
                
            </div>
            <div class="form-group">
                <label>ขั้นต่ำในการสั่งซื้อ</label>
                <input type="number" min="1" name="pro_min" class="form-control" placeholder="กรอกขั้นต่ำในการสั่งซื้อ" required>
            </div>
                <input type="hidden" name="pro_status" value="เปิดใช้งาน">
                <button class="btn btn-success" name="addpro"><span class="glyphicon glyphicon-ok"></span></button>
                <a href="index.php?link=user" class="btn btn-danger"><span class="glyphicon glyphicon-remove"></span></a>
            </div>
            <?php
                if(isset($_POST['addpro'])){
                    $sm_id=$_SESSION['id'];
                    $pro_code=$_POST['pro_code'];
                    $pro_detail=$_POST['pro_detail'];
                    $pro_dis=$_POST['pro_dis'];
                    $pro_type=$_POST['pro_type'];
                    $pro_min=$_POST['pro_min'];
                    $pro_status=$_POST['pro_status'];

                    $sql_c = "SELECT * FROM tbl_promotion WHERE pro_code='$pro_code'";
                    $result_c =mysqli_query($conn,$sql_c);
                    if(mysqli_num_rows($result_c) > 0){
                            $_SESSION['status_error'] ='โปรโมชั่นส่วนลดนี้มีในระบบแล้ว';
                            ?>
                            <script>
                                window.location.href="index.php?link=pro"
                            </script>
                            <?php
                    }else{
                        $sql = "INSERT INTO tbl_promotion(m_id,pro_code,pro_detail,pro_dis,pro_type,pro_min,pro_status) VALUES ('$sm_id','$pro_code','$pro_detail','$pro_dis','$pro_type','$pro_min','$pro_status')";
                        $result = mysqli_query($conn,$sql);
                        if($result){
                            $_SESSION['status'] = 'เพิ่มข้อมูลสำเร็จ';
                            ?>
                             <script>
                                window.location.href="index.php?link=pro"
                             </script>
                            <?php
                        }else{
                            $_SESSION['status_error'] = 'เพิ่มไม่ข้อมูลสำเร็จ';
                            ?>
                             <script>
                                window.location.href="index.php?link=pro"
                             </script>
                            <?php
                        }
                    }
                }
            ?>
        </form>
    <?php
}
?>