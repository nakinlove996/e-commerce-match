
<?php
if($_SESSION['role'] !== 'admin'){
    header('location: ../index.php');
}else{
    ?>
        <form action="" method="post" >
            <h1 >เพิ่มชือหมวดหมู่สินค้า</h1>
            <hr>
            <div class="form-group">
                <label>ชือหมวดหมู่</label>
                <input type="text" name="c_name" class="form-control" placeholder="กรอกชือหมวดหมู่" required>
                <input type="hidden" name="c_status" value="เปิดใช้งาน">
            </div>
            <div class="form-group">
                <button class="btn btn-success" name="addcat"><span class="glyphicon glyphicon-ok"></span></button>
                <a href="index.php?link=cat" class="btn btn-danger"><span class="glyphicon glyphicon-remove"></span></a>
            </div>
            <?php
                if(isset($_POST['addcat'])){
                    $c_name=$_POST['c_name'];
                    $sm_id =$_SESSION['id'];
                    $sql_c = "SELECT * FROM tbl_category WHERE c_name='$c_name' ";
                    $result_c =mysqli_query($conn,$sql_c);
                    if(mysqli_num_rows($result_c) > 0){
                            $_SESSION['status_error'] ='ชื่อหมวดหมู่นี้มีในระบบแล้ว';
                            ?>
                            <script>
                                window.location.href="index.php?link=cat"
                            </script>
                            <?php
                    }else{
                        $sql = "INSERT INTO tbl_category(c_name,m_id) VALUES ('$c_name','$sm_id')";
                        $result = mysqli_query($conn,$sql);
                        if($result){
                            $_SESSION['status'] = 'เพิ่มข้อมูลสำเร็จ';
                            ?>
                             <script>
                                window.location.href="index.php?link=cat"
                             </script>
                            <?php
                        }else{
                            $_SESSION['status_error'] = 'เพิ่มไม่ข้อมูลสำเร็จ';
                            ?>
                             <script>
                                window.location.href="index.php?link=cat"
                             </script>
                            <?php
                        }
                    }
                }
            ?>
        </form>
    <?php
}
?>