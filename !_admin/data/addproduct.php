
<?php
if($_SESSION['role'] !== 'admin'){
    header('location: ../index.php');
}else{
    $sm_id =$_SESSION['id'];
    ?>
        <form action="" method="post" enctype="multipart/form-data">
            <h1 >เพิ่มสินค้า</h1>
            <hr>
            <input type="hidden" name="m_id" value="<?=$sm_id?>">
            <div class="form-group">
                <label>หมวดหมู่สินค้า</label>
                <select name="c_id" class="form-control" required>
                    <option value="">กรุณาเลือกหมดหมู่สินค้า</option>
                    <?php
                        $sql1 = "SELECT * FROM tbl_category ";
                        $result1 =mysqli_query($conn,$sql1);
                        foreach($result1 as $row){
                            ?>
                                <option value="<?=$row['c_id']?>"><?=$row['c_name']?></option>
                            <?php
                        }
                    ?>
                </select>
            </div>
            <div class="form-group">
                <label>ชื่อสินค้า</label>
                <input type="text" name="p_name" class="form-control" placeholder="กรอกชื่อสินค้า" required>
            </div>
            <div class="form-group">
                <label>รายละเอียด</label>
                <textarea name="p_detail" rows="5" required class="form-control" placeholder="กรอกรายละเอียด"></textarea>
            </div>
            <div class="form-group">
                <label>จำนวนสินค้า</label>
                <input type="number" min="1" name="p_stock" class="form-control" placeholder="กรอกจำนวนสินค้า" required>
            </div>
            <div class="form-group">
                <label>ราคาสินค้า</label>
                <input type="number" min="1" name="p_price" class="form-control" placeholder="กรอกราคาสินค้า" required>
            </div>
            <div class="form-group">
                <label>รูปภาพสินค้า</label>
                <input type="file" name="file_img" id="file_img" class="form-control"  required>
            </div>
            <div class="form-group">
                <button class="btn btn-success" name="addp"><span class="glyphicon glyphicon-ok"></span></button>
                <a href="index.php?link=user" class="btn btn-danger"><span class="glyphicon glyphicon-remove"></span></a>
            </div>
            <?php
                if(isset($_POST['addp'])){
                    $m_id=$_POST['m_id'];
                    $c_id=$_POST['c_id'];
                    $p_name=$_POST['p_name'];
                    $p_detail=$_POST['p_detail'];
                    $p_stock=$_POST['p_stock'];
                    $p_price=$_POST['p_price'];
                    $img = $_FILES['file_img']['name'];

                    $img_array = array('image/jpg','image/jpeg','image/png','image/gif','');
                    $img_inarray = in_array($_FILES['file_img']['type'],$img_array);

                    if($img_inarray){

                            $sql = "INSERT INTO tbl_products(m_id,c_id,p_name,p_detail,p_stock,p_price,p_img) VALUES ('$m_id','$c_id','$p_name','$p_detail','$p_stock','$p_price','$img')";
                            $result = mysqli_query($conn,$sql);
                            if($result){
                                move_uploaded_file($_FILES['file_img']['tmp_name'],"../img_product/".$_FILES['file_img']['name']);
                                $_SESSION['status'] = 'เพิ่มข้อมูลสำเร็จ';
                                ?>
                                 <script>
                                    window.location.href="index.php?link=p"
                                 </script>
                                <?php
                            }else{
                                $_SESSION['status_error'] = 'เพิ่มไม่ข้อมูลสำเร็จ';
                                ?>
                                 <script>
                                    window.location.href="index.php?link=p"
                                 </script>
                                <?php
                            }
                    }else{
                        $_SESSION['status_error'] = 'เลือกไฟล์รูปที่เป็น jpg png jpeg gif *';
                        ?>
                         <script>
                            window.location.href="index.php?link=p"
                         </script>
                        <?php
                    }
                   
                }
            ?>
        </form>
    <?php
}
?>