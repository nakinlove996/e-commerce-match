
<?php
if($_SESSION['role'] !== 'admin'){
    header('location: ../index.php');
}else{
    $sm_id =$_SESSION['id'];

    $p_id = $_REQUEST['ID'];
    $sql = "SELECT * FROM tbl_products WHERE p_id='$p_id'";
    $result = mysqli_query($conn,$sql);
    $data = mysqli_fetch_array($result);

    $m_id =$data['m_id'];
    $sql2 = "SELECT * FROM tbl_members WHERE m_id='$m_id'";
    $result2 = mysqli_query($conn,$sql2);
    $data2 = mysqli_fetch_array($result2);

    $c_id = $data['c_id'];
    $sql1 = "SELECT * FROM tbl_category WHERE c_id='$c_id'";
    $result1 = mysqli_query($conn,$sql1);
    $data1 = mysqli_fetch_array($result1);
    ?>
        <form action="" method="post" enctype="multipart/form-data">
            <h1 >เพิ่มสินค้า</h1>
            <hr>
            <input type="hidden" name="m_id" value="<?=$sm_id?>">
            <center>
                <p><img src="../img_product/<?=$data['p_img']?>" alt="" width="250px" height="250px"></p>
            </center>
           
            <div class="form-group">
                <label>หมวดหมู่สินค้า</label>
                <select name="c_id" class="form-control" required>
                    <option value="<?=$data1['c_id']?>">หมวดหมู่สินค้าที่เลือก : <?=$data1['c_name']?></option>
                    <?php
                        $sql1 = "SELECT * FROM tbl_category ";
                        $result1 =mysqli_query($conn,$sql1);
                        foreach($result1 as $row){
                            ?>
                                <option value="<?=$row['c_id']?>"><?=$row['c_name']?></option>
                            <?php
                        }
                    ?>
                </select>
            </div>
            <div class="form-group">
                <label>ชื่อสินค้า</label>
                <input type="text" name="p_name" class="form-control" placeholder="กรอกชื่อสินค้า" required value="<?=$data['p_name']?>">
            </div>
            <div class="form-group">
                <label>รายละเอียด</label>
                <textarea name="p_detail" rows="5" required class="form-control" placeholder="กรอกรายละเอียด"><?=$data['p_detail']?></textarea>
            </div>
            <div class="form-group">
                <label>จำนวนสินค้า</label>
                <input type="number" min="1" name="p_stock" class="form-control" placeholder="กรอกจำนวนสินค้า" required value="<?=$data['p_stock']?>">
            </div>
            <div class="form-group">
                <label>ราคาสินค้า</label>
                <input type="number" min="1" name="p_price" class="form-control" placeholder="กรอกราคาสินค้า" required value="<?=$data['p_price']?>">
            </div>
            <div class="form-group">
                <label>รูปภาพสินค้า</label>
                <input type="file" name="file_img" id="file_img" class="form-control">
            </div>
            <div class="form-group">
                <h4>เปลี่ยนแปลงสินค้าล่าสุดโดย :<b> <?=$data2['m_name']?></b></h4>
            </div>
            <div class="form-group">
            <input type="hidden" value="<?=$data['p_id']?>" name="p_id">
                <button class="btn btn-success" name="editp"><span class="glyphicon glyphicon-ok"></span></button>
                <a href="index.php?link=p" class="btn btn-danger"><span class="glyphicon glyphicon-remove"></span></a>
            </div>
            <?php
                if(isset($_POST['editp'])){
                    $p_id=$_POST['p_id'];

                    $m_id=$_POST['m_id'];
                    $c_id=$_POST['c_id'];
                    $p_name=$_POST['p_name'];
                    $p_detail=$_POST['p_detail'];
                    $p_stock=$_POST['p_stock'];
                    $p_price=$_POST['p_price'];
                    $img = $_FILES['file_img']['name'];

                    $img_array = array('image/jpg','image/jpeg','image/png','image/gif','');
                    $img_inarray = in_array($_FILES['file_img']['type'],$img_array);

                    if($img_inarray){
                        
                        $img_c ="SELECT * FROM tbl_products WHERE p_id='$p_id'";
                        $img_r_c =mysqli_query($conn,$img_c);

                        foreach($img_r_c as $row){
                            if($img == NULL){
                                $img_data = $row['p_img'];
                            }else{
                                if($img_path="../img_product/".$row['p_img']){
                                    unlink($img_path);
                                    $img_data= $img;
                                }
                            }
                        }

                            $sql = "UPDATE tbl_products SET m_id='$m_id',c_id='$c_id',p_name='$p_name',p_detail='$p_detail',p_stock='$p_stock',p_price='$p_price',p_img='$img_data' WHERE p_id='$p_id'";
                            $result = mysqli_query($conn,$sql);
                            
                            if($result){
                                if($img == NULL){
                                    $_SESSION['status'] = 'แก้ไขข้อมูลสำเร็จ';
                                    ?>
                                     <script>
                                        window.location.href="index.php?link=p"
                                     </script>
                                    <?php
                                }else{
                                    move_uploaded_file($_FILES['file_img']['tmp_name'],"../img_product/".$_FILES['file_img']['name']);
                                    $_SESSION['status'] = 'แก้ไขข้อมูลสำเร็จ';
                                    ?>
                                     <script>
                                        window.location.href="index.php?link=p"
                                     </script>
                                    <?php
                                }
                                
                            }else{
                                $_SESSION['status_error'] = 'เพิ่มไม่ข้อมูลสำเร็จ';
                                ?>
                                 <script>
                                    window.location.href="index.php?link=p"
                                 </script>
                                <?php
                            }
                    }else{
                        $_SESSION['status_error'] = 'เลือกไฟล์รูปที่เป็น jpg png jpeg gif *';
                        ?>
                         <script>
                            window.location.href="index.php?link=p"
                         </script>
                        <?php
                    }
                   
                }
            ?>
        </form>
    <?php
}
?>