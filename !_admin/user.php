
<?php
if($_SESSION['role'] !== 'admin'){
    header('location: ../index.php');
}else{
   
    ?>
        <form action="" method="post" class="form-inline">
            <h1 align="center">ระบบจัดการสมาชิก</h1>
            <hr>
            <div class="form-group">
                <a href="index.php?link=adduser" class="btn btn-info">+เพิ่มสมาชิก</a>
            </div>

            <div class="input-group">
                <input type="search" name="su" class="form-control">
                    <div class="input-group-btn">
                        <button class="btn btn-success"><span class="glyphicon glyphicon-search"></span></button>
                    </div>
            </div>
            <br>
            <br>
            <?php
                if(isset($_SESSION['status']) && $_SESSION['status']){
                    echo '<h2 style="color:green;">'.$_SESSION['status'].'</h2>';
                    unset($_SESSION['status']);
                }
                if(isset($_SESSION['status_error']) && $_SESSION['status_error']){
                    echo '<h2 style="color:red;">'.$_SESSION['status_error'].'</h2>';
                    unset($_SESSION['status_error']);
                }
            ?>
            <div class="table-reponsive">
                <table class="table table-hover table-bordered">
                    <thead>
                        <th style="text-align:center"><h4>#</h4></th>
                        <th style="text-align:center"><h4>ชื่อ-สุกล</h4></th>
                        <th style="text-align:center"><h4>เบอร์โทร</h4></th>
                        <th style="text-align:center"><h4>อีเมล</h4></th>
                        <th style="text-align:center"><h4>ชื่อผู้ใช้</h4></th>
                        <th style="text-align:center"><h4>ตำแหน่ง</h4></th>
                        <th style="text-align:center"><h4>เพิ่มเติม</h4></th>
                    </thead>
                    <?php
                        $s = isset($_POST['su']) ? $_POST['su']:'';
                        if($s!=''){
                            $sql = "SELECT * FROM tbl_members WHERE m_id LIKE '%$s%' OR m_name LIKE '%$s%' OR m_telephone LIKE '%$s%' OR m_email LIKE '%$s%' OR m_username LIKE '%$s%' OR m_role LIKE '%$s%' ";
                            $result = mysqli_query($conn,$sql);
                        }else{
                            $query = "SELECT * FROM tbl_members" ;
                            $result = mysqli_query($conn,$query);

                        }while($row = mysqli_fetch_array($result)){

                            ?>
                                <tbody align="center" >
                                    <th style="text-align:center"><?=$row['m_id']?></th>
                                    <td><?=$row['m_name']?></td>
                                    <td><?=$row['m_telephone']?></td>
                                    <td><?=$row['m_email']?></td>
                                    <td><?=$row['m_username']?></td>
                                    <td><?=$row['m_role']?></td>
                                    </form>
                                    <td>
                                    <form action="" method="post" class="form-inline">
                                        <a href="index.php?link=edituser&ID=<?=$row['m_id']?>" class="btn btn-warning"><span class="glyphicon glyphicon-pencil"></span></a>
                                            <input type="hidden" name="m_id" value="<?=$row['m_id']?>">
                                            <button class="btn btn-danger" name="del_user" onclick="return confirm('ลบข้อมูล ?')"><span class="glyphicon glyphicon-trash"></span></button>
                                            <?php
                                                if(isset($_POST['del_user'])){
                                                    $m_id =$_POST['m_id'];
                                                    $sql ="DELETE FROM tbl_members WHERE m_id='$m_id'";
                                                    $result=mysqli_query($conn,$sql);
                                                    if($result){
                                                        $_SESSION['status'] = 'ลบสำเร็จ';
                                                        ?>
                                                        <script>
                                                            window.location.href="index.php?link=user"
                                                        </script>
                                                        <?php
                                                    }else{
                                                        $_SESSION['status_error'] = 'ลบไม่สำเร็จ';
                                                        ?>
                                                        <script>
                                                            window.location.href="index.php?link=user"
                                                        </script>
                                                        <?php
                                                    }
                                                }
                                            ?>
                                        </form>
                                    </td>
                                </tbody>
                                
                            <?php
                        }
                    ?>
                </table>
              
            </div>
          
    <?php
}
?>