
<?php
if($_SESSION['role'] !== 'admin'){
    header('location: ../index.php');
}else{
   
    ?>
        <form action="" method="post" class="form-inline">
            <h1 align="center">รายงานการขาย</h1>
            <hr>
            <?php
                $nowday = date('Y-m-d');
                $sql = "SELECT * FROM tbl_payment WHERE DATE_FORMAT(day_pay,'%Y-%m-%d') = '$nowday'";
                $res = mysqli_query($conn,$sql);
                while ($row = mysqli_fetch_array($res)){
                    $totalday = $totalday + $row['order_net'];
                }
            ?>
            <?php
                $month = date('m');
                $sql = "SELECT * FROM tbl_payment WHERE DATE_FORMAT(day_pay,'%m') = '$month'";
                $res = mysqli_query($conn,$sql);
                while ($row = mysqli_fetch_array($res)){
                    $totalm = $totalm + $row['order_net'];
                }
            ?>
            <?php
                $year = date('Y');
                // print_r($year);
                $sql = "SELECT * FROM tbl_payment WHERE DATE_FORMAT(day_pay,'%Y') = '$year'";
                $res = mysqli_query($conn,$sql);
                while ($row = mysqli_fetch_array($res)){
                    $totaly = $totaly + $row['order_net'];
                }
            ?>
            
            <div class="row">
            <a href="index.php?link=day">
            <div class="col-sm-2">
                <div class="alert alert-info" >
                   <h4><span class="glyphicon glyphicon-btc"></span> ยอดขายรายวัน</h4>
                   <hr>
                    <?=number_format($totalday,2)?> ฿</p>
                </div>
            </div>
            </a>

            <a href="index.php?link=month">
            <div class="col-sm-2">
                <div class="alert alert-success" >
                    <h4><span class="glyphicon glyphicon-btc"></span> ยอดขายรายเดือน</h4><hr>
                    <?=number_format($totalm,2)?> ฿</p>
                </div>
            </div>
            </a>

            <a href="index.php?link=year">
                <div class="col-sm-2">
                    <div class="alert alert-danger">
                    <h4><span class="glyphicon glyphicon-btc"></span> ยอดขายรายปี</h4> <hr>
                    <?=number_format($totaly,2)?> ฿
                    </div>
                </div>
            </a>
           
            </div>
            
            
            
            <label for="">คนหาการขายรายวัน : </label>
                <input type="date" class="form-control" name="sr" onchange="this.form.acion='';this.form.submit();">
            
            
            <br>
            <br>
            <div class="table-reponsive">
                <table class="table table-hover table-bordered">
                    <thead>
                        <th style="text-align:center" ><h4>เลขใบสั่งซื้อ</h4></th>
                        <th style="text-align:center"><h4>รูปแบบการจัดส่ง</h4></th>
                        <th style="text-align:center"><h4>วันที่ชำระ</h4></th>
                        <th style="text-align:center"><h4>วันที่สั่งซื้อ</h4></th>
                        <th style="text-align:center"><h4>ราคาสินค้า</h4></th>
                        <th style="text-align:center"><h4>เพิ่มเติม : ออเดอร์การสั่งซื้อ</h4></th>
                    </thead>
                    <?php
                    $sum = 0 ;
                        $s = isset($_POST['sr']) ? $_POST['sr']:'';
                        if($s!=''){
                            $sql = "SELECT * FROM tbl_payment WHERE day_pay LIKE '%$s%' ";
                            $result = mysqli_query($conn,$sql);
                        }else{
                            
                            $sql = "SELECT * FROM tbl_payment ";
                            $result = mysqli_query($conn,$sql);
                        }while($row = mysqli_fetch_array($result)){
                            $order_id = $row['order_id'];
                            $sql1 = "SELECT * FROM tbl_order WHERE order_id='$order_id'";
                            $result1 = mysqli_query($conn,$sql1);
                            $data1 = mysqli_fetch_array($result1);
                            ?>
                                <tbody align="center" >
                                    <th style="text-align:center"><?=$data1['order_id']?></th>
                                    <td><?=$data1['order_ship']?></td>
                                    <td><?=$row['day_pay']?></td>
                                    <td><?=$data1['order_date']?></td>
                                    <td><?=number_format($data1['order_net'],2)?></td>
                                    
                                    
                                    </form>
                                    <td width="15%">
                                        <button onclick="window.open('../receipt.php?order_id=<?=$data1['order_id']?>','_popup','width=500px,height=600px')" class="btn btn-success"><span class="glyphicon glyphicon-eye-open"></span></button>
                                    </td>
                                </tbody>
                            <?php
                            $sum = $sum + $row['order_net'];
                        }
                    ?>
                    <tr>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td align="center"><b>ยอดรวมสุทธิ</b></td>
                        <td align="center"><?=number_format($sum,2)?> </td>
                        <td align="center"><b>บาท</b></td>
                    </tr>
                </table>
             
            </div>
          
    <?php
}
?>