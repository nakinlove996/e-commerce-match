
<?php
if($_SESSION['role'] !== 'admin'){
    header('location: ../index.php');
}else{
   
    ?>
        <form action="" method="post" class="form-inline">
            <h1 align="center">แสดงการสั่งซื้อ</h1>
            <hr>
            <select name="sv" class="form-control" onchange="this.form.submit();">
                <option value="">แสดงทั้งหมด</option>
                <option>สั่งซื้อเรียบร้อย/รอชำระ</option>
                <option>ชำระเรียบร้อย/รอตรวจสอบ</option>
                <option>ตรวจสอบเรียบร้อย/รอจัดส่ง</option>
                <option>จัดส่งสินค้าเรียบร้อย</option>
                <option>ยกเลิก</option>
            </select>
            <br>
            <br>
            <div class="table-reponsive">
                <table class="table table-hover table-bordered">
                    <thead>
                        <th style="text-align:center"><h4>เลขใบสั่งซื้อ</h4></th>
                        <th style="text-align:center"><h4>รูปแบบการจัดส่ง</h4></th>
                        <th style="text-align:center"><h4>วันที่สั่งซื้อ</h4></th>
                        <th style="text-align:center"><h4>สถานะสินค้า</h4></th>
                        <th style="text-align:center"><h4>ราคาสินค้า</h4></th>
                        <th style="text-align:center"><h4>เพิ่มเติม : ออเดอร์การสั่งซื้อ</h4></th>
                    </thead>
                    <?php
                        $s = isset($_POST['sv']) ? $_POST['sv']:'';
                        if($s!=''){
                            $sql = "SELECT * FROM tbl_order WHERE order_status LIKE '%$s%' ";
                            $result = mysqli_query($conn,$sql);
                        }else{
                            $sql = "SELECT * FROM tbl_order";
                            $result = mysqli_query($conn,$sql);
                        }while($row = mysqli_fetch_array($result)){

                            ?>
                                <tbody align="center" >
                                    <th style="text-align:center"><?=$row['order_id']?></th>
                                    <td><?=$row['order_ship']?></td>
                                    <td><?=$row['order_date']?></td>
                                    <td><?=$row['order_status']?></td>
                                    <td><?=number_format($row['order_net'],2)?></td>
                                    </form>
                                    <td>
                                        <button onclick="window.open('../receipt.php?order_id=<?=$row['order_id']?>','_blank')" class="btn btn-success"><span class="glyphicon glyphicon-eye-open"></span></button>
                                    </td>
                                </tbody>
                            <?php
                        }
                    ?>
                </table>
              
            </div>
          
    <?php
}
?>