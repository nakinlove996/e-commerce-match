<?php
session_start();
error_reporting(error_reporting() & ~E_NOTICE);
if($_SESSION['role'] !== 'admin'){
    header('location: ../index.php');
}else{
    include 'server/conn.php';
    ?>
    <!DOCTYPE html>
    <html lang="en">
    <head>
        <?php include 'include/h.php'; ?>
    </head>
    <body>
    <?php include 'include/navbar.php'; ?>
    <!-- <button onclick="topFunction()" id="myBtn" title="Go to top" style="display: block;margin-top:150px;">
        <span class="glyphicon glyphicon-chevron-up" aria-hidden="true">ssss</span>
    </button> -->
    <div class="padding-l-r">
        <div class="row">
            <div class="col-sm-2">
                <?php include 'include/menu_left.php'; ?>
            </div>
            <br>
            <div class="col-sm-10">
                <div class="border-table shadow-lg">
                    <?php
                        $link = $_GET['link'];
                        if($link == ''){
                            include 'home.php';
                        }elseif($link == 'user'){
                            include 'user.php';
                        }elseif($link == 'adduser'){
                            include 'data/adduser.php';
                        }elseif($link == 'edituser'){
                            include 'data/edituser.php';
                        }elseif($link == 'cat'){
                            include 'cat.php';
                        }elseif($link == 'addcat'){
                            include 'data/addcat.php';
                        }elseif($link == 'editcat'){
                            include 'data/editcat.php';
                        }elseif($link == 'p'){
                            include 'product.php';
                        }elseif($link == 'addp'){
                            include 'data/addproduct.php';
                        }elseif($link == 'editp'){
                            include 'data/editproduct.php';
                        }elseif($link == 'pro'){
                            include 'promotion.php';
                        }elseif($link == 'addpro'){
                            include 'data/addpromotion.php';
                        }elseif($link == 'editpro'){
                            include 'data/editpromotion.php';
                        }elseif($link == 'view'){
                            include 'view.php';
                        }elseif($link == 'order'){
                            include 'order.php';
                        }elseif($link == 'report'){
                            include 'report.php';
                        }elseif($link == 'day'){
                            include 'r_daily.php';
                        }elseif($link == 'month'){
                            include 'r_monthy.php';
                        }elseif($link == 'year'){
                            include 'r_yearly.php';
                        }
                    ?>
                </div>
            </div>
        </div>
    </div>
        <?php include 'include/f.php'; ?>
    </body>
    </html>
    <?php
}
?>