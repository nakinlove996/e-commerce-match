
            <?php
            $perpage = 12;
            if (isset($_GET['page'])) {
            $page = $_GET['page'];
            } else {
            $page = 1;
            }
            $start = ($page - 1) * $perpage;
            //  echo $start;
            $query = "SELECT detail_pay,order_net, SUM(order_net) AS totol, DATE_FORMAT(day_pay, '%d-%M-%Y') AS day_pay
            FROM tbl_payment
            GROUP BY DATE_FORMAT(day_pay, '%d%')
            ORDER BY DATE_FORMAT(day_pay, '%Y-%m-%d') DESC
            ";
            $result = mysqli_query($conn, $query);
           ?>
           <a href="index.php?link=report" class="btn btn-danger"><span class="glyphicon glyphicon-arrow-left"></span> กลับ</a>
                <h3>ยอดการขายต่อวัน</h3>
                <table  class="table table-striped" border="1" cellpadding="0"  cellspacing="0" align="center">
                    <thead>
                        <tr class="info">
                            <th width="30%"><h4>ว/ด/ป</h4></th>
                            <th width="70%"><center><h4>รายได้</h4></center></th>
                        </tr>
                    </thead>
                    
                    
                    <?php 
					
					while($row2 = mysqli_fetch_array($result)) { 
					
					?>
                    <tr>
                        <td><?php echo $row2['day_pay'];?></td>
                        <td align="right"><?php echo number_format($row2['order_net'],2);?></td>
                    </tr>
                    <?php
                    @$amount_total += $row2['order_net'];
                    }
                    ?>
                </table>
               