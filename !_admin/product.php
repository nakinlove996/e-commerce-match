
<?php
if($_SESSION['role'] !== 'admin'){
    header('location: ../index.php');
}else{
    ?>
        <form action="" method="post" class="form-inline">
            <h1 align="center">ระบบจัดการสินค้า</h1>
            <hr>
            <div class="form-group">
                <a href="index.php?link=addp" class="btn btn-info">+เพิ่มสินค้า</a>
            </div>
            <button class="btn btn-warning" name="num"><span class="glyphicon glyphicon-info-sing"></span>แสดงสินค้าที่น้อยกว่าหรือเท่ากับ10ชิ้น </button>
            <div class="input-group">
                <input type="search" name="sp" class="form-control">
                    <div class="input-group-btn">
                        <button class="btn btn-success"><span class="glyphicon glyphicon-search"></span></button>
                    </div>
            </div>
            <br>
            <br>
            <?php
                if(isset($_SESSION['status']) && $_SESSION['status']){
                    
                    echo '<div class="alert alert-success" role="alert">
                    <a href="#" class="alert-link">'.$_SESSION['status'].'</a>
                  </div>';
                    unset($_SESSION['status']);
                }
                if(isset($_SESSION['status_error']) && $_SESSION['status_error']){
                    echo '<h2 style="color:red;">'.$_SESSION['status_error'].'</h2>';
                    unset($_SESSION['status_error']);
                }
            ?>
            <div class="table-reponsive">
                <table class="table table-hover table-bordered">
                    <thead>
                        <th style="text-align:center"><h4>#</h4></th>
                        <th style="text-align:center"><h4>รูปสินค้า</h4></th>
                        <th style="text-align:center"><h4>หมวดหมู่</h4></th>
                        <th style="text-align:center"><h4>ชื่อสินค้า</h4></th>
                        <th style="text-align:center"><h4>จำนวนสินค้า</h4></th>
                        <th style="text-align:center"><h4>ราคาสินค้า</h4></th>
                        <th style="text-align:center"><h4>เพิ่มเติม</h4></th>
                    </thead>
                    <?php
                        $s = isset($_POST['sp']) ? $_POST['sp']:'';
                        if($s!=''){
                            $sql = "SELECT * FROM tbl_products WHERE p_id LIKE '%$s%' OR p_name LIKE '%$s%' OR p_stock LIKE '%$s%' OR p_price LIKE '%$s%' ";
                            $result = mysqli_query($conn,$sql);
                        }elseif(isset($_POST['num'])){
                            $sql = "SELECT * FROM tbl_products WHERE p_stock <= 10";
                            $result = mysqli_query($conn,$sql);
                        }else{
                         
                            $query = "SELECT * FROM tbl_products";
                            $result = mysqli_query($conn,$query);
                            
                        }while($row = mysqli_fetch_array($result)){
                            $c_id =$row['c_id'];
                            $sql1 = "SELECT * FROM tbl_category WHERE c_id='$c_id'";
                            $result1 = mysqli_query($conn,$sql1);
                            $data1 =mysqli_fetch_array($result1);

                            $m_id =$row['m_id'];
                            $sql2 = "SELECT * FROM tbl_members WHERE m_id='$m_id'";
                            $result2 = mysqli_query($conn,$sql2);
                            $data2 =mysqli_fetch_array($result2);
                            ?>
                                <tbody align="center">
                                    <td><?=$row['p_id']?></td>
                                    <td align="left" width="18%" >
                                        <a href="../img_product/<?=$row['p_img']?>" data-lightbox="photos">
                                        <img src="../img_product/<?=$row['p_img']?>" alt="" width="100px" height="100px">
                                    </a>
                                    <br>
                                    <small class="pull-right">เปลี่ยนแปลงครั้งล่าสุดโดย : <d style="color:green;"><?=$data2['m_name']?></d></small> </td>
                                    <td><?=$data1['c_name']?></td>
                                    <td><?=$row['p_name']?></td>
                                    <td>
                                        <?php
                                            if($row['p_stock'] <1 ){
                                                ?>
                                                    <span class="label label-danger">สินค้าหมด</span>
                                                <?php
                                            }elseif($row['p_stock'] <= 5){
                                                ?>
                                                    <?=$row['p_stock']?> ชิ้น <span class="label label-warning">สินค้าไกล้หมด</span>
                                                <?php
                                            }elseif($row['p_stock'] <=10){
                                                ?>
                                                    <?=$row['p_stock']?> ชิ้น <span class="label label-info">สินค้าเหลือน้อย</span>
                                                <?php
                                            }else{
                                                ?>
                                                    <?=$row['p_stock']?> ชิ้น
                                                    <?php
                                            }
                                        ?>
                                    </td>
                                    <td><?=number_format($row['p_price'],2)?> ฿</td>
                                    </form>
                                    <td>
                                    <form action="" method="post" class="form-inline">
                                        <a href="index.php?link=editp&ID=<?=$row['p_id']?>" class="btn btn-warning"><span class="glyphicon glyphicon-pencil"></span></a>
                                            <input type="hidden" name="p_id" value="<?=$row['p_id']?>">
                                            <button class="btn btn-danger" name="del_p" onclick="return confirm('ลบข้อมูล ?')"><span class="glyphicon glyphicon-trash"></span></button>
                                            <?php
                                                if(isset($_POST['del_p'])){
                                                    $p_id =$_POST['p_id'];
                                                    $sql ="DELETE FROM tbl_products WHERE p_id='$p_id'";
                                                    $result=mysqli_query($conn,$sql);
                                                    if($result){
                                                        $_SESSION['status'] = 'ลบสำเร็จ';
                                                        ?>
                                                        <script>
                                                            window.location.href="index.php?link=p"
                                                        </script>
                                                        <?php
                                                    }else{
                                                        $_SESSION['status_error'] = 'ลบไม่สำเร็จ';
                                                        ?>
                                                        <script>
                                                            window.location.href="index.php?link=p"
                                                        </script>
                                                        <?php
                                                    }
                                                }
                                            ?>
                                        </form>
                                    </td>
                                    
                                </tbody>
                            <?php
                        }
                    ?>
                </table>
              
            </div>
          
    <?php
}
?>