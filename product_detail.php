<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Product</title>
    <link rel="stylesheet" href="main.css">
    <link rel="stylesheet" href="bootstrap/css/bootstrap.css">
    <script src="jquery/jquery-3.5.0.min.js"></script>
    <script src="bootstrap/js/boostrap.min.js"></script>

    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Pridi&display=swap" rel="stylesheet">
</head>
<body>
    
    <?php include('includes/condb.php') ?>
    <?php include('includes/navbar.php') ?>

    <?php 
    
    $p_id = $_GET['p_id'];

    $sql = "SELECT * FROM tbl_products WHERE p_id = '$p_id'";
    $query = mysqli_query($conn, $sql);
    $row = mysqli_fetch_array($query);

    $view = $row['p_view'];
    $count_view = $view + 1;

    $sql_u = "UPDATE tbl_products SET p_view = '$count_view'";
    $query_u = mysqli_query($conn, $sql_u);

    ?>

    <div class="container">
        <div class="row">
            <div class="col-sm-6">
                <img src="img_product/<?=$row['p_img']?>" style="width:100%">
            </div>
            <div class="col-sm-6">
                <h2><?=$row['p_name']?></h2>
                <div>จำนวนเข้าชม <span class="badge"><?=$row['p_view']?></span> ครั้ง</div><hr>
                <div><?=nl2br($row['p_detail'])?></div><br>
                <h3><div>ราคา <?=number_format($row['p_price'])?> บาท </h3>
                <div>สินค้าคงเหลือ <?=$row['p_stock']?> ชิ้น</div>
                <div class="col-sm-3">
                    <input type="number" class="form-control" min="1" max="<?=$row['p_stock']?>" value="1">
                </div>
                <div>
                    <?php if($row['p_stock'] < 1 ){ ?>
                    <button class="btn btn-success disabled">สินค้าหมด</button>
                    <?php }elseif(isset($_SESSION['id'])){ ?>
                    <button class="btn btn-success">หยิบใส่ตะกร้า</button>
                    <?php }else{ ?>
                    <button class="btn btn-success disabled">หยิบใส่ตะกร้า</button>
                    <?php } ?>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                        
                <?php include('rec.php') ?>

            </div>
        </div>
    </div>

    <?php include('includes/footer.php') ?>

</body>
</html>