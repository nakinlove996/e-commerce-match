-- phpMyAdmin SQL Dump
-- version 5.0.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: May 08, 2021 at 09:20 PM
-- Server version: 10.2.34-MariaDB-log
-- PHP Version: 7.3.27

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `redwolf_`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_cart`
--

CREATE TABLE `tbl_cart` (
  `cart_id` int(5) NOT NULL,
  `cart_sid` text NOT NULL,
  `p_id` int(5) NOT NULL,
  `cart_num` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_category`
--

CREATE TABLE `tbl_category` (
  `c_id` int(11) NOT NULL,
  `c_name` varchar(50) NOT NULL,
  `m_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_category`
--

INSERT INTO `tbl_category` (`c_id`, `c_name`, `m_id`) VALUES
(1, 'ร้องเท้า', 0),
(2, 'ถุงเท้า', 0),
(3, 'เสื้อแขนยาว', 5),
(4, 'HTLM', 5),
(5, 'Css', 5),
(6, 'React JS', 5),
(7, 'Php', 5),
(8, 'Java Scripts', 5);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_members`
--

CREATE TABLE `tbl_members` (
  `m_id` int(5) NOT NULL,
  `m_name` varchar(150) NOT NULL,
  `m_telephone` varchar(10) NOT NULL,
  `m_email` varchar(200) NOT NULL,
  `m_address` text NOT NULL,
  `m_username` varchar(50) NOT NULL,
  `m_password` varchar(50) NOT NULL,
  `m_q_pass` varchar(50) NOT NULL,
  `m_a_pass` varchar(50) NOT NULL,
  `m_role` varchar(30) NOT NULL,
  `m_date` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_members`
--

INSERT INTO `tbl_members` (`m_id`, `m_name`, `m_telephone`, `m_email`, `m_address`, `m_username`, `m_password`, `m_q_pass`, `m_a_pass`, `m_role`, `m_date`) VALUES
(2, 'ฟหกฟหกกกsadas', '1111111111', 'somrot262@gmail.com', 'ddd', 'dddd', 'dddd', 'เลขที่ชอบ', 'ddd', 'member', '2020-12-12 09:04:58'),
(3, 'นคินทร์ บุญสิงมาa', '0000000000', 'ddd@ddddd', 'sssss', 'a', 'ss', 'เลขที่ชอบ', 'ss', 'admin', '2020-12-12 18:01:13'),
(4, 'นคินทร์ บุญสิงมาa', '0000000000', 'ddd@dd', 'sss', 'a', 'a', 'เลขที่ชอบ', 'a', 'admin', '2020-12-12 18:02:12'),
(5, 'ccc', '1111111111', 'cccc@c', 'c', 'c', 'c', 'สีที่ชอบ', 'c', 'admin', '2020-12-12 19:33:04');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_order`
--

CREATE TABLE `tbl_order` (
  `order_id` int(5) NOT NULL,
  `m_id` int(5) NOT NULL,
  `pro_id` int(5) NOT NULL,
  `order_dis` int(10) NOT NULL,
  `order_ship` varchar(30) NOT NULL,
  `cost_ship` int(10) NOT NULL,
  `order_net` int(10) NOT NULL,
  `order_status` varchar(150) NOT NULL,
  `order_date` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_order`
--

INSERT INTO `tbl_order` (`order_id`, `m_id`, `pro_id`, `order_dis`, `order_ship`, `cost_ship`, `order_net`, `order_status`, `order_date`) VALUES
(1, 1, 0, 50, 'Kerry', 150, 1500, 'จัดส่งสินค้าเรียบร้อย', '2020-12-12 09:58:21');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_payment`
--

CREATE TABLE `tbl_payment` (
  `id_pay` int(5) NOT NULL,
  `order_id` int(5) NOT NULL,
  `m_id` int(5) NOT NULL,
  `order_net` int(10) NOT NULL,
  `net_pay` int(10) NOT NULL,
  `time_pay` time NOT NULL,
  `day_pay` date NOT NULL,
  `bank_pay` varchar(150) NOT NULL,
  `detail_pay` varchar(150) NOT NULL,
  `img_pay` text NOT NULL,
  `date_pay` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_payment`
--

INSERT INTO `tbl_payment` (`id_pay`, `order_id`, `m_id`, `order_net`, `net_pay`, `time_pay`, `day_pay`, `bank_pay`, `detail_pay`, `img_pay`, `date_pay`) VALUES
(1, 1, 1, 1500, 1, '40:00:00', '2020-12-12', '', '', '', '2020-12-12 10:05:14');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_products`
--

CREATE TABLE `tbl_products` (
  `p_id` int(5) NOT NULL,
  `m_id` int(5) NOT NULL,
  `c_id` int(5) NOT NULL,
  `p_name` varchar(150) NOT NULL,
  `p_detail` text NOT NULL,
  `p_stock` int(10) NOT NULL,
  `p_price` int(10) NOT NULL,
  `p_view` int(10) NOT NULL,
  `p_img` text NOT NULL,
  `p_date` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_products`
--

INSERT INTO `tbl_products` (`p_id`, `m_id`, `c_id`, `p_name`, `p_detail`, `p_stock`, `p_price`, `p_view`, `p_img`, `p_date`) VALUES
(10, 5, 4, 'Basiac Html', 'uu', 5, 500, 34, '1200px-HTML5_logo_resized.svg.png', '2020-12-12 18:46:36'),
(11, 5, 1, 'basic Css', 'uuu', 15, 500, 34, 'CSS3_logo_and_wordmark.svg.png', '2020-12-12 19:00:03'),
(12, 5, 8, 'basic JavaScripts', 'uu', 5, 500, 34, '1024px-Unofficial_JavaScript_logo_2.svg.png', '2020-12-12 19:31:26'),
(13, 5, 6, 'basic React Js', 'basic React Js', 10, 500, 34, 'download.png', '2021-05-01 13:21:14');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_promotion`
--

CREATE TABLE `tbl_promotion` (
  `pro_id` int(5) NOT NULL,
  `m_id` int(5) NOT NULL,
  `pro_code` varchar(30) NOT NULL,
  `pro_detail` text NOT NULL,
  `pro_dis` int(10) NOT NULL,
  `pro_type` varchar(30) NOT NULL,
  `pro_min` int(10) NOT NULL,
  `pro_status` varchar(50) NOT NULL,
  `pro_date` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_promotion`
--

INSERT INTO `tbl_promotion` (`pro_id`, `m_id`, `pro_code`, `pro_detail`, `pro_dis`, `pro_type`, `pro_min`, `pro_status`, `pro_date`) VALUES
(1, 4, 'as50', '', 50, 'บาท', 500, 'เปิดใช้งาน', '2020-12-12 09:42:56'),
(2, 5, 'HAPPY50', 'asdasd', 50, 'บาท', 5000, 'ปิดใช้งาน', '2020-12-12 19:15:48');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_receipt`
--

CREATE TABLE `tbl_receipt` (
  `re_id` int(5) NOT NULL,
  `p_id` int(5) NOT NULL,
  `order_id` int(5) NOT NULL,
  `cart_num` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_shipping`
--

CREATE TABLE `tbl_shipping` (
  `ship_id` int(5) NOT NULL,
  `ship_track` varchar(30) NOT NULL,
  `order_id` int(5) NOT NULL,
  `ship_data` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_shipping`
--

INSERT INTO `tbl_shipping` (`ship_id`, `ship_track`, `order_id`, `ship_data`) VALUES
(1, 'ssss', 1, '2020-12-12 11:02:55');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_cart`
--
ALTER TABLE `tbl_cart`
  ADD PRIMARY KEY (`cart_id`);

--
-- Indexes for table `tbl_category`
--
ALTER TABLE `tbl_category`
  ADD PRIMARY KEY (`c_id`);

--
-- Indexes for table `tbl_members`
--
ALTER TABLE `tbl_members`
  ADD PRIMARY KEY (`m_id`);

--
-- Indexes for table `tbl_order`
--
ALTER TABLE `tbl_order`
  ADD PRIMARY KEY (`order_id`);

--
-- Indexes for table `tbl_payment`
--
ALTER TABLE `tbl_payment`
  ADD PRIMARY KEY (`id_pay`);

--
-- Indexes for table `tbl_products`
--
ALTER TABLE `tbl_products`
  ADD PRIMARY KEY (`p_id`);

--
-- Indexes for table `tbl_promotion`
--
ALTER TABLE `tbl_promotion`
  ADD PRIMARY KEY (`pro_id`);

--
-- Indexes for table `tbl_receipt`
--
ALTER TABLE `tbl_receipt`
  ADD PRIMARY KEY (`re_id`);

--
-- Indexes for table `tbl_shipping`
--
ALTER TABLE `tbl_shipping`
  ADD PRIMARY KEY (`ship_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_cart`
--
ALTER TABLE `tbl_cart`
  MODIFY `cart_id` int(5) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_category`
--
ALTER TABLE `tbl_category`
  MODIFY `c_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `tbl_members`
--
ALTER TABLE `tbl_members`
  MODIFY `m_id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `tbl_order`
--
ALTER TABLE `tbl_order`
  MODIFY `order_id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tbl_payment`
--
ALTER TABLE `tbl_payment`
  MODIFY `id_pay` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tbl_products`
--
ALTER TABLE `tbl_products`
  MODIFY `p_id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `tbl_promotion`
--
ALTER TABLE `tbl_promotion`
  MODIFY `pro_id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tbl_receipt`
--
ALTER TABLE `tbl_receipt`
  MODIFY `re_id` int(5) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_shipping`
--
ALTER TABLE `tbl_shipping`
  MODIFY `ship_id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
