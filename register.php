<div class="modal fade" id="myRegister" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <h3 align="center">สมัครสมาชิก</h3>
                <form action="code.php" method="post">
                    <div class="form-group">
                        <label class="control-label">Name</label>
                        <input class="form-control" type="text" name="name" placeholder="Name" required>
                    </div>
                    <div class="form-group">
                        <label class="control-label">คำถามกู้คืนรหัสผ่าน</label>
                        <select name="m_q_pass" class="form-control">
                            <option>คำถามกู้คืนรหัสผ่าน</option>
                            <option value="สีที่ชอบ">สีที่ชอบ</option>
                            <option value="เลขที่ชอบ">เลขที่ชอบ</option>
                            <option value="อาหารโปรด">อาหารโปรด</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label class="control-label">คำตอบกู้คืนรหัสผ่าน</label>
                        <input class="form-control" type="text" name="m_a_pass" placeholder="คำตอบกู้คืนรหัสผ่าน" required>
                    </div>
                    <div class="form-group">
                        <label class="control-label">Username</label>
                        <input class="form-control" type="text" name="username" placeholder="Username" required>
                    </div>
                    <div class="form-group">
                        <label class="control-label">Password</label>
                        <input class="form-control" type="password" name="password" placeholder="Password" required>
                    </div>
                    <div class="form-group">
                        <label class="control-label">Password</label>
                        <input class="form-control" type="password" name="c_password" placeholder="ConfirmPassword" required>
                    </div>
                    <div class="form-group">
                        <label class="control-label">Email</label>
                        <input class="form-control" type="text" name="email" placeholder="Email" required>
                    </div>
                    <div class="form-group">
                        <label class="control-label">Telephone</label>
                        <input class="form-control" type="text" name="telephone" placeholder="Telephone" required>
                    </div>
                    <div class="form-group">
                        <label class="control-label">Address</label>
                        <input type="hidden" name="role" value="member">
                        <textarea name="address" class="form-control" rows="4" placeholder="Address" required></textarea>
                    </div>
                    <div class="form-group" align="center">
                        <button class="btn btn-success" name="btn_reg">ตกลง</button>
                        <button class="btn btn-danger" data-dismiss="modal">ยกเลิก</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>