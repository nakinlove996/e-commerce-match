<div class="modal fade" id="myRecov" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <h3 align="center">กู้คืนรหัสผ่าน</h3>
                <form action="code.php" method="post">
                    <div class="form-group">
                        <label class="control-label">Username</label>
                        <input type="text" class="form-control" name="username" placeholder="Username" require>
                    </div>
                    <div class="form-group">
                        <label class="control-label">คำถามกู้คืนรหัสผ่าน</label>
                        <select name="m_q_pass" class="form-control">
                            <option>คำถามกู้คืนรหัสผ่าน</option>
                            <option value="สีที่ชอบ">สีที่ชอบ</option>
                            <option value="เลขที่ชอบ">เลขที่ชอบ</option>
                            <option value="อาหารโปรด">อาหารโปรด</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label class="control-label">คำตอบกู้คืนรหัสผ่าน</label>
                        <input type="text" class="form-control" name="m_a_pass" placeholder="คำตอบกู้คืนรหัสผ่าน" require>
                    </div>
                    <div class="form-group">
                        <button class="btn btn-success" name="btn_recov">ตกลง</button>
                        <button class="btn btn-danger" data-dismiss="modal">ยกเลิก</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>