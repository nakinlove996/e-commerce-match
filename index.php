<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Home</title>
    <link rel="stylesheet" href="main.css">
    <link rel="stylesheet" href="bootstrap/css/bootstrap.css">
    <script src="jquery/jquery-3.5.0.min.js"></script>
    <script src="bootstrap/js/bootstrap.min.js"></script>

    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Pridi&display=swap" rel="stylesheet">
</head>
<body>
    
    <?php include('includes/condb.php') ?>
    <?php include('includes/navbar.php') ?>

    <div class="container">
        <div class="row">
            <div class="col-lg-10 col-xs-10">
            <!-- <div class="col-lg-10 col-md-10 col-sm-9 col-xs-10 col-xl-offset-3"> -->

                <?php include('pop_product.php') ?>

            </div>
            <div class="col-lg-2">
            <!-- <div class="col-lg-2 col-md-2 col-sm-3 col-xs-12"> -->

                <?php include('category.php') ?>

            </div>
        </div>
        <div class="row">
            <div class="col-sm-10">
            
                <?php include('show_product.php') ?>

            </div>
        </div>
    </div>

    <?php include('includes/footer.php') ?>

</body>
</html>