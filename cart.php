<?php session_start(); ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Cart</title>
    <link rel="stylesheet" href="main.css">
    <link rel="stylesheet" href="bootstrap/css/bootstrap.css">
    <script src="jquery/jquery-3.5.0.min.js"></script>
    <script src="bootstrap/js/bootstrap.min.js"></script>
</head>
<body>
    
    <?php include('includes/navbar.php') ?>

    <div class="container">
        <div class="row">
            <table class="table table-hover">
                <thead>
                    <tr>
                        <th>สินค้า</th>
                        <th>จำนวน</th>
                        <th>ราคา</th>
                        <th>ราคารวม</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                <?php
                include('includes/condb.php');

                $s_id = $_SESSION['s_id'];
                $allsum = 0;
                $sql = "SELECT * FROM tbl_cart WHERE cart_sid = '$s_id'";
                $query = mysqli_query($conn, $sql);
                $num = mysqli_num_rows($query);

                if($num == 0){
                    ?>
                    <script>
                    window.location.href='index.php';
                    </script>
                    <?php
                }

                while($row = mysqli_fetch_array($query)){

                    $p_id = $row['p_id'];

                    $sql_p = "SELECT * FROM tbl_products WHERE p_id = '$p_id'";
                    $query_p = mysqli_query($conn, $sql_p);
                    $row_p = mysqli_fetch_array($query_p);

                    $sum = $row_p['p_price'] * $row['cart_num'];

                ?>
                    <tr>
                        <td>
                            <div>
                                <img src="img_product/<?=$row_p['p_img']?>" width="72px" height="72px">
                                <span><?=$row_p['p_name']?></span>
                            </div>
                        </td>

                        <td width="15%">
                            <div class="col-sm-7" style="margin-top:5px; margin-left: -24px">
                                <form action="" method="post">
                                    <input type="hidden" name="cart_id" value=<?=$row['cart_id']?>>
                                    <input type="hidden" name="p_stock" value=<?=$row_p['p_stock']?>>
                                    <input type="number" name="cart_num" value=<?=$row['cart_num']?> onchange="this.form.action='';this.form.submit();" min="1" max="<?=$row_p['p_stock']?>" class="form-control">
                                </form>
                            </div>
                        </td>

                        <td width="15%"><div style="margin-top:10px"><?=number_format($row_p['p_price'],2)?> ฿</div></td>
                        <td><div style="margin-top:10px"><?=number_format($sum,2)?> ฿</div></td>

                        <td>
                            <div>
                                <form action="" method="post">
                                    <input type="hidden" name="cart_id" value="<?=$row['cart_id']?>">
                                    <button class="btn btn-danger" name="btn_del"><span class="glyphicon glyphicon-remove"></span></button>
                                </form>
                            </div>
                        </td>
                    </tr>

                    <?php
                    
                        $allsum = $sum + $allsum;
                    }

                    if(isset($_POST['cart_num'])){
                        $cart_id = $_POST['cart_id'];
                        $cart_num = $_POST['cart_num'];
                        $p_stock = $_POST['p_stock'];

                        if($cart_num > $p_stock){
                            $cart_num = $p_stock;
                        }elseif($cart_num < 1){
                            $sql_delete = "DELETE FROM tbl_cart WHERE cart_id = '$cart_id'";
                            mysqli_query($conn, $sql_delete);
                            ?>
                            <script>window.location.href='cart.php'</script>
                            <?php
                        }

                        $sql_update = "UPDATE tbl_cart SET cart_num = '$cart_num' WHERE cart_id ='$cart_id'";
                        mysqli_query($conn, $sql_update);
                        ?>
                        <script>window.location.href='cart.php'</script>
                        <?php

                    }

                    if(isset($_POST['btn_del'])){
                        $cart_id = $_POST['cart_id'];
                        $sql_del = "DELETE FROM tbl_cart WHERE cart_id = '$cart_id'";
                        mysqli_query($conn, $sql_del);
                        ?>
                        <script>window.location.href='cart.php';</script>
                        <?php
                    }

                    ?>

                    <tr>
                        <td>
                            <form action="" method="post">
                                <div class="input-group col-sm-6">
                                    <input type="text" name="pro_code" class="form-control" placeholder="ใส่ CODE ส่วนลดที่นี่">
                                    <div class="input-group-btn">
                                        <button type="submit" name="btn_usepro" class="btn btn-primary">USE</button>
                                    </div>
                                </div>
                            </form>
                        </td>
                        <?php
                        $pro_detail = '';
                        $pro_dis = 0;
                        $pro_id = 0;

                        if(isset($_POST['btn_usepro'])){

                            $pro_code = $_POST['pro_code'];

                            $sql_pro = "SELECT * FROM tbl_promotion WHERE pro_code = '$pro_code'";
                            $query_pro = mysqli_query($conn, $sql_pro);
                            $row_pro = mysqli_fetch_array($query_pro);
                            $num_pro = mysqli_num_rows($query_pro);

                            if($num_pro == 0){
                                ?>
                                <script>
                                alert('ไม่พบ CODE นี้');
                                window.location.href='cart.php';
                                </script>
                                <?php
                            }else{
                                if($row_pro['pro_status'] <> 'เปิดใช้งาน'){
                                    ?>
                                    <script>
                                    alert('CODE หมดอายุการใช้งานแล้ว');
                                    window.location.href='cart.php';
                                    </script>
                                    <?php
                                }else{
                                    
                                    if($allsum < $row_pro['pro_min']){
                                        ?>
                                        <script>
                                        alert('ราคารวมยังไม่ถึงขั้นต่ำของโปรโมชั่น');
                                        window.location.href='cart.php';
                                        </script>
                                        <?php
                                    }else{

                                        if($row_pro['pro_type'] == 'เปอร์เซ็น'){
                                            $pro_dis = $allsum * ($row_pro['pro_dis']/100);
                                        }else{
                                            $pro_dis = $row_pro['pro_dis'];
                                        }

                                        $pro_id = $row_pro['pro_id'];
                                        $pro_detail = $row_pro['pro_detail'];

                                    }
                                }
                            }
                        }

                        $net = $allsum - $pro_dis;
                        ?>

                        <td></td>
                        <td></td>
                        <td><div style="margin-top:7px;">ราคารวม</div></td>
                        <td style="text-align:right;"><div style="margin-top:7px;"><?=number_format($allsum,2)?> ฿</div></td>
                    </tr>
                    <tr>
                        <td><?=$pro_detail?></td>
                        <td></td>
                        <td></td>
                        <td><div>ส่วนลด</div></td>
                        <td style="text-align:right;"><div><?=number_format($pro_dis,2)?> ฿</div></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td><div>ราคาสุทธิ</div></td>
                        <td style="text-align:right;"><div><?=number_format($net,2)?> ฿</div></td>
                    </tr>
                    <tr>
                        <td><div><a href="index.php" class="btn btn-default">เลือกสินเพิ่มเติม</a></div></td>
                        <td></td>
                        <td></td>

                        <td>
                            <div>
                                <form action="" method="post">
                                    <button name="btn_del_all" type="submit" class="btn btn-danger">ลบสินค้าทั้งหมด</button>
                                </form>
                            </div>
                        </td>

                        <?php
                        
                        if(isset($_POST['btn_del_all'])){

                            $sql_del_all = "DELETE FROM tbl_cart";
                            mysqli_query($conn, $sql_del_all); ?>

                            <script>window.location.href='index.php';</script>

                        <?php } ?>

                        <td>
                            <div>
                                <form action="confirm_address.php" method="post">
                                    <input type="hidden" name="pro_id" value="<?=$pro_id?>">
                                    <input type="hidden" name="pro_dis" value="<?=$pro_dis?>">
                                    <button class="btn btn-success">ดำเนินการต่อ</button>
                                </form>
                            </div>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>

    <?php include('includes/footer.php') ?>

</body>
</html>