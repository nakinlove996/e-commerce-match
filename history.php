<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="main.css">
    <link rel="stylesheet" href="bootstrap/css/bootstrap.css">
    <script src="jquery/jquery-3.5.0.min.js"></script>
    <script src="bootstrap/js/bootstrap.min.js"></script>
</head>
<body>

<?php include_once('includes/navbar.php'); ?>

<?php
include_once('includes/condb.php'); //เรียกใช้ไฟล์ config
$user = isset($_SESSION['user'])?$_SESSION['user']:''; //ดึงค่า user ที่เก็บไว้บน session มาใช้
$id = $_SESSION['id'];
$sql = "SELECT * FROM tbl_order WHERE $id = m_id ORDER BY order_id DESC";
$result = mysqli_query($conn, $sql); //เรียกใช้ตารางออเดอร์ เรียงแบบมากไปน้อย
$no = 1; //ลำดับที่
?>
<h3 align="center">ประวัติการสั่งซื้อสินค้า</h3><hr>
<div class="container">
	<table class="table table-striped table-hover table-bordered" style="margin-top: 15px;">
    	<thead>
        	<tr>
            	<th></th>
                <th>เลขที่ใบสั่งซื้อ</th>
                <th>วันที่สั่งซื้อ</th>
                <th>รูปแบบการส่ง</th>
                <th>ราคาสินค้า</th>
                <th>สถานะสินค้า</th>
                <th style="text-align:center;">เพิ่มเติม</th>
            </tr>
        </thead>
        <tbody>
        <?php
        while($data=mysqli_fetch_array($result)) //แสดงรายละเอียดออเดอร์ แล้ว loop ออเดอร์ของผู้ใช้งานทั้งหมด
		{
		?>
        	<tr>
            	<td><?=$no?></td> <!--ลำดับที่-->
                <td><?=$data['order_id']?></td> <!--เลขที่ใบสั่งซื้อ-->
                <td><?=$data['order_date']?></td> <!--วันที่สั่งซื้อ-->
                <td><?=$data['order_ship']?></td> <!--รูปแบบการส่ง-->
                <td><?=number_format($data['order_net'])?> ฿</td> <!--ราคาสินค้า-->
                <td><?=$data['order_status']?></td> <!--สถานะสินค้า-->
                <td style="text-align:center;">
                
                <?php
                if($data['order_status']=='แจ้งชำระ/รอตรวจสอบ' or $data['order_status']=='ตรวจสอบเรียบร้อย/รอจัดส่ง') //เงื่อนไขถ้า สถานะสินค้าเป็น แจ้งชำระ/รอตรวจสอบ หรือ ตรวจสอบเรียบร้อย/รอจัดส่ง
				{
				?>
                <button class="btn btn-success disabled">แจ้งชำระเงิน</button> <!--ไม่สามารถกดปุ่มแจ้งชำระได้-->
                <?php
				}
				elseif($data['order_status']=='จัดส่งสินค้าเรียบร้อย') //เงื่อนไขถ้าสถานะสินค้าเป็น จัดส่งสินค้าเรียบร้อย
				{
				?>
				<button onclick="window.open('member/view_ship.php?order_id=<?=$data['order_id']?>','_popup','width=520px height=755px');return false;" class="btn btn-warning">ดูเลขพัสดุ</button> <!-- สมารถกดปุ่มดูเลขพัสดุได้-->	
				<?php
                }
				else //เงื่อนไขถ้า สถานะสินค้าไม่ตรงกับสถานะที่กล่าวถึงข้างต้น
				{
				?>
                <button class="btn btn-success" onclick="window.open('payment.php?order_id=<?=$data['order_id']?>','_popup','width=520px height=755px');return false;">แจ้งชำระเงิน</button> <!--สามารถกดแจ้งชำระเงินได้-->
                <?php
				}
				?>
                <button class="btn btn-info" onclick="window.open('receipt.php?order_id=<?=$data['order_id']?>','_blank')">ดูรายละเอียด</button> <!--สำหรับดูรายละเอียดการสั่งซื้อ-->
                </td>
            </tr>
        <?php
			$no++; //การ +1 ลำดับที่
		}
		?>
        </tbody>
    </table>
</div>

<?php include_once('includes/footer.php') ?>

</body>
</html>