<?php session_start(); //เปิดใช้คำสั่ง session

include_once('includes/condb.php'); //เรียกใช้ไฟล์ config
$user = isset($_SESSION['username'])?$_SESSION['username']:''; //ดึงค่า user ที่เก็บไว้บน session มาใช้
$sql = "SELECT * FROM tbl_members WHERE m_username = '$user'";
$result = mysqli_query($conn, $sql); //เรียกใช้ตาราง member เมื่อ user_m เท่ากับ ตัวปแร user ที่รับมา
$data = mysqli_fetch_array($result); //แสดงข้อมูล

$order_id = $_GET['order_id']; //รับค่า GET ของ order_id จากการแจ้งชำระของหน้า history
$m_id = $data['m_id']; //ตัวแปร m_id
// print_r($_GET);
// print_r($data);
$sql_or = "SELECT * FROM tbl_order WHERE order_id = '$order_id'";
$query_or = mysqli_query($conn, $sql_or); //เรียกใช้ตาราง order เมื่อ order_id เท่ากับ ตัวแปร order_id ที่รับมา
$row_or = mysqli_fetch_array($query_or); //แสดงข้อมูลของตาราง order
$order_net = $row_or['order_net']; //ตัวแปรราคารวมสินค้า
// print_r($row_or);
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Payment</title>
    <link rel="stylesheet" href="main.css">
    <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
    <script src="jquery/jquery-3.5.0.min.js"></script>
    <script src="bootstrap/js/bootstrap.min.js"></script>
</head>
<body>
    
<div class="container">

    <h4>ชำระเงินผ่าน ATM InternetBanking</h4>
    <hr>
    <div class="form-group">เลขที่ใบสั่งซื้อ :
        <span><?=$order_id?></span> <!--แสดงเลขที่ใบสั่งซื้อ-->
    </div>
    <div class="form-group">ชื่อ-สกุล :
        <span><?=$data['m_name']?> </span> <!--แสดงชื่อ นามสกุลผู้ใช้งาน-->
    </div>
    <div class="form-group">วันที่สั่งซื้อ :
        <span><?=$row_or['order_date']?></span> <!--แสดงวันที่สั่งซื้อสินค้า-->
    </div>
    <div class="form-group">ราคารวม :
        <span><?=number_format($order_net)?> บาท</span> <!--แสดงราคารวมของสินค้า-->
    </div>
    
    <form action="" method="post" enctype="multipart/form-data">
    <div class="form-group">
        <label class="control-label">ธนาคารที่ชำระ :</label>
        <select name="bank_pay" class="form-control">
        <option>---- เลือกธนาคารที่ชำระ ----</option>
        <option>ธนาคารกสิกรไทย</option>
        <option>ธนาคารกรุงไทย</option>
        <option>ธนาคารกรุงเทพ</option>        
        </select>
    </div>
    <div class="form-group">
        <label class="control-label">จำนวนเงินที่ชำระ :</label>
        <input type="text" class="form-control" required placeholder="ใส่จำนวนเงิน" name="net_pay">
    </div>
    <div class="form-group">
        <label class="control-label">เวลาที่ชำระ :</label>
        <input type="time" class="form-control" required name="time_pay">
    </div>
    <div class="form-group">
        <label class="control-label">วันที่ชำระ :</label>
        <input type="date" class="form-control" required name="day_pay">
    </div>
    <div class="form-group">
        <label class="control-label">หลักฐานการชำระ :</label>
        <input name="img_pay" type="file" class="form-control" required>
    </div>
    <div class="form-group">
        <label class="control-label">ข้อความ :</label>
        <textarea class="form-control" name="detail_pay"></textarea>
    </div>
    <div align="center"><button class="btn btn-success" type="submit" name="insert">แจ้งชำระ</button> <button class="btn btn-danger" onClick="window.close()">ยกเลิก</button></div>                
    </form>
    </div>
    <?php
    if(isset($_POST['insert'])) //รับค่ามาจากปุ่ม insert
    {
    $bank_pay = $_POST['bank_pay']; //ชื่อธานคารที่กรอก
    $day_pay = $_POST['day_pay']; //วันที่โอนเงิน
    $time_pay = $_POST['time_pay']; //เวลาที่โอน
    $detail_pay = $_POST['detail_pay']; //รายละเอียด
    $net_pay = $_POST['net_pay']; //จำนวนเงินที่โอน
    $img_pay = $_FILES['img_pay']['name']; //หลักฐานการชำระ

    move_uploaded_file($_FILES['img_pay']['tmp_name'],"img_pay/".$img_pay); //การเก็บรูปหลักฐานการชำระไว้ใน folder slip

    $sql = "INSERT INTO tbl_payment (order_id,m_id,order_net,bank_pay,day_pay,time_pay,detail_pay,net_pay,img_pay) VALUES ('$order_id','$m_id','$order_net','$bank_pay','$day_pay','$time_pay','$detail_pay','$net_pay','$img_pay')";
    $result = mysqli_query($conn,$sql);
    
    if($result){
        $sql_update = "UPDATE tbl_order SET order_status = 'แจ้งชำระ/รอตรวจสอบ' WHERE order_id = '$order_id'";
        $query_update = mysqli_query($conn, $sql_update); //คำสั่ง update status_or ในตาราง order เมื่อ order_id เท่ากับ ตัวแปร order_id ที่รับมา

        if($query_update){
            ?>
                <script>
                window.close(); //คำสั่งปิดหน้าต่าง
                </script>
        <?php
        }
        
    }else{
        
    }
   

    ?>
    <script>
    // window.close(); //คำสั่งปิดหน้าต่าง
    </script>
    <?php
    }
    ?>
 
            
           
</body>
</html>