<?php session_start(); ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
	<link rel="stylesheet" href="main.css">
    <link rel="stylesheet" href="bootstrap/css/bootstrap.css">
    <script src="jquery/jquery-3.5.0.min.js"></script>
    <script src="bootstrap/js/bootstrap.min.js"></script>
</head>
<body>

<?php include_once('includes/navbar.php'); ?>

<?php
$ship = $_POST['ship'];
$ship_cost = $_POST['ship_cost'];
$pro_dis = $_POST['pro_dis'];
$pro_id = $_POST['pro_id'];
?>
<body>
<div class="container">
	<div class="row">
    	<table class="table table-hover">
        	<thead>
            	<tr>
                	<th>สินค้า</th>
                    <th>จำนวน</th>
                    <th>ราคา</th>
                    <th>ราคารวม</th>
                </tr>
            </thead>
            <tbody>
            <?php
			include_once('includes/condb.php'); // ส่วนของการแสดงรายการสินค้า คล้ายกับระบบตะกร้า สามารถอ้างอิงได้เลย 
			
			$m_id = $_SESSION['id'];
			$s_id = $_SESSION['s_id'];
			$allsum = 0;
			$sql = "SELECT * FROM tbl_cart WHERE cart_sid = '$s_id'";
			$result = mysqli_query($conn, $sql);
			$num = mysqli_num_rows($result);
			
			if($num==0)
			{
				?>
				<script>
				window.location.href='aaa.php';
				</script>
				<?php
			}
			
			while($row = mysqli_fetch_array($result))
			{
				$p_id = $row['p_id'];
				$sql_p = "SELECT * FROM tbl_products WHERE p_id = '$p_id'";
				$query_p = mysqli_query($conn, $sql_p);
				$row_p = mysqli_fetch_array($query_p);
				
				$sum = $row_p['p_price'] * $row['cart_num'];
            ?>
            	<tr>
                	<td width="45%"><div><img src="img_product/<?=$row_p['p_img']?>" width="10%"><span> <?=$row_p['p_name']?></span></div></td>
                    <td width="15%"><div class="col-sm-7"><?=$row['cart_num']?></div></td>
                    <td width="15%"><div><?=number_format($row_p['p_price'],2)?> ฿</div></td>
                    <td><div><?=number_format($sum,2)?> ฿</div></td>
                    <td></td>
                </tr>
            <?php
				$allsum = $sum + $allsum;
			}
			
			$vat = $allsum * 0.07;
			$novat = $allsum - $vat;
			
			$sql_pro = "SELECT * FROM tbl_promotion WHERE pro_id = '$pro_id'";
			$query_pro = mysqli_query($conn, $sql_pro);
			$row_pro = mysqli_fetch_array($query_pro);
			$num_pro = mysqli_num_rows($query_pro);
			$pro_detail = '';			
						if($num_pro <> 0)
						{
							$pro_detail="คุณได้ใช้ส่วนลด ".$row_pro['pro_detail'];
						}
						
			$net = $allsum - $pro_dis + $ship_cost;
			?>
                <tr>
                	<td><?= $pro_detail?></td>
                    <td></td>
                    <td></td>
                    <td><div style="text-align: right;">ราคาไม่รวมภาษี</div></td>
                    <td><div style="text-align: right;"><?=number_format($novat,2)?> ฿</div></td>
                </tr>
                <tr>
                	<td></td>
                    <td></td>
                    <td></td>
                    <td><div style="text-align: right;">ภาษีมูลค่าเพิ่ม</div></td>
                    <td><div style="text-align: right;"><?=number_format($vat,2)?> ฿</div></td>
                </tr>                                
                <tr>
                	<td></td>
                    <td></td>
                    <td></td>
                    <td><div style="text-align: right;">ราคารวม</div></td>
                    <td><div style="text-align: right;"><?=number_format($allsum,2)?> ฿</div></td>
                </tr>
                <tr>
                	<td></td>
                    <td></td>
                    <td></td>
                    <td><div style="text-align: right;">ส่วนลด</div></td>
                    <td><div style="text-align: right;"><?=number_format($pro_dis,2)?> ฿</div></td>
                </tr> 
                <tr>
                	<td></td>
                    <td></td>
                    <td></td>
                    <td><div style="text-align: right;">ค่าจัดส่ง</div></td>
                    <td><div style="text-align: right;"><?=number_format($ship_cost,2)?> ฿</div></td>
                </tr>                
                <tr>
                	<td></td>
                    <td></td>
                    <td></td>
                    <td><h4 style="text-align: right;">ราคาสุทธิ</h4></td>
                    <td><h4 style="text-align: right;"><?=number_format($net,2)?> ฿</h4></td>
                </tr>
                <tr>
                	<td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td><div><form action="" method="post">
        
                    <input name="pro_id" type="hidden" value="<?=$pro_id?>">
                    <input name="pro_dis" type="hidden" value="<?=$pro_dis?>">
                    <input name="ship" type="hidden" value="<?=$ship?>">
                    <input name="ship_cost" type="hidden" value="<?=$ship_cost?>">
                    <button name="btn_confirm" class="btn btn-success">ยืนยันการสั่งซื้อ <span class="glyphicon glyphicon-play"></span></button>
                    </form></div></td>
                </tr>                                                 
            </tbody>
        </table>
    </div>
</div>
<?php
if(isset($_POST['btn_confirm'])) // ถ้าหากกดปุ่ม ยืนยันสั่งซื้อ (ส่งค่าตัวแปร POST ในชื่อตัวแปรว่า add) ให้เข้าเงื่อนไขนี้
{
	$ship = $_POST['ship'];
	$ship_cost = $_POST['ship_cost'];
	$pro_dis = $_POST['pro_dis'];
	$pro_id = $_POST['pro_id'];
	
	$sql_insert = "INSERT INTO tbl_order (m_id, pro_id, order_dis, order_ship, cost_ship, order_net, order_status)
	VALUES ('$m_id', '$pro_id', '$pro_dis', '$ship', '$ship_cost', '$net', 'สั่งซื้อสินค้า/รอชำระเงิน')";
	mysqli_query($conn, $sql_insert); // เพิ่มข้อมูลลงในตาราง order 

	$sql_or = "SELECT * FROM tbl_order WHERE order_date = '$date'";
	$query_or = mysqli_query($conn, $sql_or); // เรียกใช้งาน order ที่เพิ่งเพิ่มไปล่าสุด เพื่อนำ order_id มาเก็บในตาราง order_desc อีกที
	$row_or = mysqli_fetch_array($query_or);
	
	
	
	$sql_cart = "SELECT * FROM tbl_cart WHERE cart_sid = '$s_id'";
	$query_cart = mysqli_query($conn, $sql_cart);
	$num_cart = mysqli_num_rows($query_cart); // นับแถวในตาง cart
	
	for($i=0; $i < $num_cart; $i++)
	{	
		$order_id = $row_or['order_id'];
		$row_cart = mysqli_fetch_array($query_cart);
		$p_id = $row_cart['p_id'];
		$cart_num = $row_cart['cart_num'];
		
		$sql_re = "INSERT INTO tbl_receipt (order_id, p_id, cart_num) VALUES ('$order_id', '$p_id', '$cart_num')";
		mysqli_query($conn, $sql_re); // เพิ่มสินค้าใน order_desc
		
		$sql_de = "DELETE FROM tbl_cart WHERE p_id = '$p_id'";
		mysqli_query($conn, $sql_de); // ลบสินค้าในตะกร้า
	}
	
	?>
	<script>
	window.location.href='history.php';
	</script>
	<?php
}
?>

<?php include('includes/footer.php'); ?>

</body>
</html>