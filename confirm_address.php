<?php session_start(); ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="main.css">
    <link rel="stylesheet" href="bootstrap/css/bootstrap.css">
    <script src="jquery/jquery-3.5.0.min.js"></script>
    <script src="bootstrap/js/bootstrap.min.js"></script>
</head>
<body>

<?php include('includes/navbar.php'); ?>

<br/>
<br/>

<?php
include_once('includes/condb.php');
$username = $_SESSION['username']; // เรียกใช้งาน รับค่า session ที่ตอนเข้าสู่ระบบเก็บไว้
$sql = "SELECT * FROM tbl_members WHERE m_username = '$username'";
$query = mysqli_query($conn, $sql); // เรียกใช้งานตาราง member เพื่อดึงข้อมูลของ user คนนั้น ๆ
$row = mysqli_fetch_array($query);

$pro_id = $_POST['pro_id'];
$pro_dis = $_POST['pro_dis'];


$ship = isset($_POST['ship'])?$_POST['ship']:'';

if($ship <> '')
{
	if($ship=='dhl')
	{
		$ship_cost=60;
		$text_ship='จัดส่งโดย DHL';
	}
	elseif($ship=='flash')
	{
		$ship_cost=70;
		$text_ship='จัดส่งโดย Flash';
	}
	elseif($ship=='kerry')
	{
		$ship_cost=80;
		$text_ship='จัดส่งโดย Kerry';
	}
}
else
{
	$ship = 'kerry';
	$ship_cost = 80;
	$text_ship='จัดส่งโดย Kerry';
}

?>
<body>
<div class="container">

        <div class="panel panel-default">
            <div class="panel-heading"><h4>ข้อมูลในการจัดส่ง</h4></div>
            <div class="panel-body">
            	<div class="col-sm-6" style="border-left: solid 1px #eeeeee; border-right: solid 1px #eeeeee;"><strong>ชื่อผู้รับ :</strong> คุณ<span><?=$row['m_name']?> </span>
                	<br>
                	<strong>หมายเลขโทรศัพท์ :</strong> <span><?=$row['m_telephone']?></span>
                    <br>
                    <strong>อีเมล :</strong> <span><?=$row['m_email']?></span>
                    </div>
               
                <div class="col-sm-6">ที่อยู่สำหรับจัดส่ง <?=$row['m_address']?>
                </div>
            </div>
        </div>
        
       <div class="panel panel-default">
            <div class="panel-heading"><h4>รูปแบบการจัดส่ง</h4></div>
            <div class="panel-body">
            	<div class="col-sm-6">
                <form action="" method="post">
                <input name="pro_id" type="hidden" value="<?=$pro_id?>">
                <input name="pro_dis" type="hidden" value="<?=$pro_dis?>">
                <input onClick="this.form.submit();" name="ship" type="radio" value="kerry"> จัดส่งโดย Kerry
                <br>
                <input onClick="this.form.submit();" name="ship" type="radio" value="flash"> จัดส่งโดย Flash
                <br>
                <input onClick="this.form.submit();" name="ship" type="radio" value="dhl"> จัดส่งโดย DHL
                <br>
                </form>
                </div>
                <div class="col-sm-offset-4 col-sm-4"><?=$text_ship?>  ค่าจัดส่ง <?=number_format($ship_cost,2)?> บาท</div>
            </div>
        </div>
        <div align="right"><form action="confirm_order.php" method="post">
        
                    <input name="pro_id" type="hidden" value="<?=$pro_id?>">
                    <input name="pro_dis" type="hidden" value="<?=$pro_dis?>">
                    <input name="ship" type="hidden" value="<?=$ship?>">
                    <input name="ship_cost" type="hidden" value="<?=$ship_cost?>">
                    <button class="btn btn-success">ดำเนินการต่อ <span class="glyphicon glyphicon-play"></span></button>
                    </form></div>
    </div>


<?php include('includes/footer.php');?>

</body>
</html>