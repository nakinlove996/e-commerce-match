<?php
if(!isset($_SESSION)){
    session_start();
}
    include('includes/condb.php');
?>

<!-- Login -->
<?php

if(isset($_POST['btn_login'])){

    $username = $_POST['username'];
    $password = $_POST['password'];

    $sql = "SELECT * FROM tbl_members WHERE m_username = '$username' AND m_password = '$password'";
    $query = mysqli_query($conn, $sql);

    if($num = mysqli_num_rows($query) < 1 ){
        ?>
        <script>
            alert('ชื่อผู้ใช้หรือรหัสผ่านไม่ถูกต้อง');
            window.location.href="index.php";
        </script>
        <?php
    }else{
        
        $row = mysqli_fetch_array($query);

        $_SESSION['id'] = $row['m_id'];
        $_SESSION['s_id'] = session_id();
        $_SESSION['name'] = $row['m_name'];
        $_SESSION['username'] = $row['m_username'];
        $_SESSION['role'] = $row['m_role'];
        
        if($_SESSION['role'] == 'admin'){
            ?>
            <script>
                alert('ยินดีต้อนรับแอดมิน <?=$_SESSION['name']?>');
                window.location.href="!_admin/index.php";
            </script>
            <?php
        }else{
            ?>
            <script>
                alert('ยินดีต้อนรับคุณ <?=$_SESSION['name']?>');
                window.location.href="index.php";
            </script>
            <?php
        }

    }

}

?>

<!-- Edit -->
<?php

if(isset($_POST['btn_edit'])){

    $username = $_POST['username'];
    $name = $_POST['name'];
    $email = $_POST['email'];
    $telephone = $_POST['telephone'];
    $address = $_POST['address'];

    $sql = "SELECT * FROM tbl_members WHERE m_username = '$username'";
    $query = mysqli_query($conn, $sql);

    if($num = mysqli_num_rows($query) == 1 ){

        $sql_update = "UPDATE tbl_members SET m_name = '$name', m_email = '$email', m_telephone = '$telephone', m_address = '$address'";
        $query_update = mysqli_query($conn, $sql_update);

        if($query_update){
            ?>
            <script>
                alert('แก้ไขข้อมูลสำเร็จ');
                window.location.href="index.php";
            </script>
            <?php
        }else{
            ?>
            <script>
                alert('แก้ไขข้อมูลไม่สำเร็จ');
                window.location.href="index.php";
            </script>
            <?php
        }

    }else{
        ?>
        <script>
            alert('แก้ไขข้อมูลไม่สำเร็จ');
            window.location.href="index.php";
        </script>
        <?php
    }

}

?>

<!-- Recover -->
<?php

if(isset($_POST['btn_recov'])){

    $username = $_POST['username'];
    $m_q_pass = $_POST['m_q_pass'];
    $m_a_pass = $_POST['m_a_pass'];

    $sql_u = "SELECT * FROM tbl_members WHERE m_username = '$username'";
    $query_u = mysqli_query($conn, $sql_u);

    if($num_u = mysqli_num_rows($query_u) < 1 ){
        ?>
        <script>
            alert('ไม่มีชื่อผู้ใช้นี้ในระบบ');
            window.location.href="index.php";
        </script>
        <?php
    }else{

        $sql = "SELECT * FROM tbl_members WHERE m_username = '$username' AND m_q_pass = '$m_q_pass' AND m_a_pass = '$m_a_pass'";
        $query = mysqli_query($conn, $sql);
        
        if($num = mysqli_num_rows($query) < 1 ){
            ?>
            <script>
                alert('คำถามหรือคำตอบกู้คืนหรัสผ่านไม่ถูกต้อง');
                window.location.href="index.php";
            </script>
            <?php
        }else{
            $row = mysqli_fetch_array($query);
            ?>
            <script>
                alert('รหัสผ่านของคุณคือ <?=$row['m_password']?>');
                window.location.href="index.php";
            </script>
            <?php
        }

    }

}

?>

<!-- Reg -->
<?php

if(isset($_POST['btn_reg'])){

    $name = $_POST['name'];
    $m_q_pass = $_POST['m_q_pass'];
    $m_a_pass = $_POST['m_a_pass'];
    $username = $_POST['username'];
    $password = $_POST['password'];
    $c_password = $_POST['c_password'];
    $email = $_POST['email'];
    $telephone = $_POST['telephone'];
    $address = $_POST['address'];
    $role = $_POST['role'];

    $sql_u = "SELECT * FROM tbl_members WHERE m_username = '$username' OR m_email = '$email'";
    $query_u = mysqli_query($conn, $sql_u);

    if($num_u = mysqli_num_rows($query_u) > 0 ){
        ?>
        <script>
            alert('ชื่อผู้ใช้หรืออีเมลนี้มีอยู่แล้ว');
            window.location.href="index.php";
        </script>
        <?php
    }else{

       if($password === $c_password){

            $sql = "INSERT INTO tbl_members (m_name, m_q_pass, m_a_pass, m_username, m_password, m_email, m_telephone, m_address, m_role)
            VALUES('$name', '$m_q_pass', '$m_a_pass', '$username', '$password', '$email', '$telephone', '$address', '$role')";
            $query = mysqli_query($conn, $sql);

            if($query){
                ?>
                <script>
                    alert('สมัครสมาชิกสำเร็จ');
                    window.location.href="index.php";
                </script>
                <?php
            }else{
                ?>
                <script>
                    alert('สมัครสมาชิกไม่สำเร็จ');
                    window.location.href="index.php";
                </script>
                <?php
            }

       }else{
           ?>
           <script>
               alert('รหัสผ่านไม่ตรงกัน');
               window.location.href="index.php";
           </script>
           <?php
       }

    }

}

?>