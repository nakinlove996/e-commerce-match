<?php
if(!isset($_SESSION)){
    session_start();
}
    include("includes/condb.php");
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    
    <div class="navbar navbar-default">
        <div class="container">
            <div class="navbar-header">
                <a class="navbar-brand" href="index.php"><img src="banner/logo.png" width="100px" height="25px"></a>
            </div>

            <ul class="nav navbar-nav">
                <li><a href="index.php">หน้าแรก</a></li>
                <li><a href="all_product.php">สินค้า</a></li>
                    
            </ul>

            <form action="" method="post" class="navbar-form navbar-left">
                <div class="form-group">
                    <input type="text" class="form-control"  name="s" placeholder="Search">
                </div>
                <button class="btn btn-default">ค้นหา</button>
            </form>

            <ul class="nav navbar-nav navbar-right">
                <?php if(isset($_SESSION['id'])){ ?>
                    <?php
                    $sid = $_SESSION['s_id'];
                    $sql = "SELECT * FROM tbl_cart WHERE cart_sid = '$sid'";
                    $query = mysqli_query($conn, $sql);
                    $stock = mysqli_num_rows($query);
                    ?>
                <li><a href="cart.php"><span class="badge"><?=$stock?></span><span class="glyphicon glyphicon-shopping-cart"></span></a></li>
                <li data-toggle="modal" data-target="#myProfile"><a href="#">คุณ <?=$_SESSION['name']?></a></li>
                <li><a href="logout.php">ออกจากระบบ</a></li>
                <?php }else{ ?>
                <li data-toggle="modal" data-target="#myLogin"><a href="#">เข้าสู่ระบบ</a></li>
                <li data-toggle="modal" data-target="#myRegister"><a href="#">สมัครสมาชิก</a></li>
                <?php } ?>
            </ul>
        </div>
    </div>

    <div class="carousel slide" id="myCarousel" data-ride="carousel" data-interval="4000">
        <ol class="carousel-indicators">
            <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
            <li data-target="#myCarousel" data-slide-to="1"></li>
        </ol>

        <div class="carousel-inner">
            <div class="item active">
                <img src="banner/banner01.jpg" style="width:100%">
            </div>
            <div class="item">
                <img src="banner/banner02.jpg" style="width:100%">
            </div>
        </div>

        <a class="left carousel-control" href="#myCarousel" data-slide="prev">
            <span class="glyphicon glyphicon-chevron-left"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="right carousel-control" href="#myCarousel" data-slide="next">
            <span class="glyphicon glyphicon-chevron-right"></span>
            <span class="sr-only">Next</span>
        </a>
    </div>

    <?php include('login.php') ?>
    <?php include('profile.php') ?>
    <?php include('register.php') ?>

    <br>
    <br>

</body>
</html>

