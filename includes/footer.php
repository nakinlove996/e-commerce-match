<footer class="site-footer">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <h1>N&N Shop</h1>
            </div>
        </div>
        <hr>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <p>Copyright &copy; 2020 All Reserved by Narongsak & Nakin</p>
            </div>
        </div>
    </div>
</footer>